#ifndef _FEU_
#define _FEU_

#include "renderable.h"
#include "particule.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#ifndef __APPLE__
#include <GL/glut.h>
#else
#include <GLUT/glut.h>
#endif

#define MAX_PARTICULES 1000

class Feu : public Renderable {
public:
    void init();
    void reinit();
    void draw();
    void animateFeu();
    Feu(float taille);

private:
    float taille;
    Particule * particules[MAX_PARTICULES];

};

#endif
