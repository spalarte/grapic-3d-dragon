#include <iostream>
using namespace std;
#include "landscape.h"
#include "textures_util.h"

#define PI 3.141592

/* Couleurs de dragon */
#define skin_R 0.5
#define skin_G 1
#define skin_B 0.8
#define wing_R 0.2
#define wing_G 1
#define wing_B 1

void Landscape::draw() {
    /* Ajout de brouillard */
    GLfloat fog_color[4] = {0.5, 0.5, 0.5, 1};
    glEnable(GL_FOG);
    glFogi(GL_FOG_MODE, GL_EXP2); // Equation utilisée pour le brouillard
    glFogf(GL_FOG_DENSITY, 0.02); // Densité (1 : complet, 0 : transparent)
    glFogfv(GL_FOG_COLOR, fog_color);
    glHint(GL_FOG_HINT, GL_NICEST);

    glPushMatrix();
    /* Fond de la scène */
    drawLandscape();
    /* Le dragon */
    drawDragon();

    glPopMatrix();
}

void Landscape::drawDragon() {

    glTranslatef(-this->rayon * sin(angle), this->rayon * cos(angle), this->hauteur);
    glRotatef(360 * angle / (2 * PI), 0, 0, 1);
    glRotatef(-20, 0, 1, 0);
    glRotatef(-20, 1, 0, 0);

    /* Le corps */
    glPushMatrix();
    glRotatef(180, 1, 0, 0);
    glTranslatef(-0.1, 0, -0.65);
    glScalef(0.9, 1, 1);
    body->draw();
    glPopMatrix();

    /* Le cou */
    glPushMatrix();
    glTranslatef(-1.9, 0, 0);
    glRotatef(-100, 0, 1, 0);
    glTranslatef(0.2, 0, 0);
    glScalef(0.5, 0.5, 0.5);
    neck->draw();

    /* La tête */
    glTranslatef(0.2, 0, 4);
    glRotatef(20, 0, 1, 0);
    tete->draw();

    /* Le feu */
    glTranslatef(3, 0, 0);
    glRotatef(90, 0, 1, 0);
    feu->draw();
    glPopMatrix();

    //le collier
    glPushMatrix();
    glTranslatef(-2.6, 0, 0.1);
    glRotatef(90, 0, 1, 0);
    glColor3f(0.3, 0.3, 0.3);
    glutSolidTorus(0.1, 0.5, 8, 8);
    glPopMatrix();

    //La chaine
    glPushMatrix();
    glTranslatef(-2.6, 0, 0.5);
    glRotatef(90, 0, 0, 1);
    glRotatef(160, 0, 1, 0);
    glRotatef(-20, 1, 0, 0);
    leash->draw();
    glPopMatrix();

    /* La queue */
    glPushMatrix();
    glTranslatef(1.9, 0, 0);
    glRotatef(120, 0, 1, 0);
    tail->draw();
    glPopMatrix();

    /* Les ailes */
    glPushMatrix();
    glTranslatef(-1.8, 0, 0);
    glRotatef(90, 1, 0, 0);
    glRotatef(90, 0, 0, 1);
    glRotatef(-90, 0, 1, 0);
    glPushMatrix();
    glTranslatef(0.5, 0, 0.3);
    glRotatef(30, 0, 0, 1);
    wing->draw();
    glPopMatrix();

    glScalef(1.0, 1.0, -1.0);
    glRotatef(180, 0, 1, 0);
    glTranslatef(0.5, 0, 0.3);
    glRotatef(30, 0, 0, 1);
    wing->draw();
    glPopMatrix();

    /* La première jambe */
    glPushMatrix();
    glTranslatef(1.3, 0.5, 0.9);
    glRotatef(-70, 0, 1, 0);
    glRotatef(20, 0, 0, 1);
    glRotatef(-10, 1, 0, 0);
    leg->draw();
    glPopMatrix();

    /* La première patte */
    glPushMatrix();
    glTranslatef(3.4, 0.6, 1.3);
    glRotatef(-30, 0, 1, 0);
    glRotatef(10, 0, 0, 1);
    patte->draw();
    glPopMatrix();

    /* La deuxième jambe */
    glPushMatrix();
    glTranslatef(1.3, -0.6, 0.9);
    glRotatef(-70, 0, 1, 0);
    glRotatef(-20, 0, 0, 1);
    glRotatef(10, 1, 0, 0);
    leg->draw();
    glPopMatrix();

    /* La deuxième patte */
    glTranslatef(3.4, -0.6, 1.3);
    glRotatef(-30, 0, 1, 0);
    glRotatef(-10, 0, 0, 1);
    patte->draw();
}

void Landscape::init(Viewer &viewer) {
    this->cycle = 0;
    this->rayon = 20;
    this->angle = 0;
    this->hauteur = -1.7;
    this->sens = 0.1;
    defaultGravity = qglviewer::Vec(0.0, 0.0, -10.0);
    gravity = defaultGravity;
    defaultMediumViscosity = 0.1;
    mediumViscosity = defaultMediumViscosity;
    handleCollisions = true;
    dt = 0.1;
    groundPosition = qglviewer::Vec(0.0, 0.0, 0.0);
    groundNormal = qglviewer::Vec(0.0, 0.0, 1.0);
    rebound = 0.5;

    /* Chargement des textures */
    tex_peau = loadTexture("textures/Reptiles_skin_repetition.jpg");
    tex_corne = loadTexture("textures/Marble_repetition.jpg");
    tex_landscape = loadTexture("textures/Tempete.jpg");
    tex_aile = loadTexture("textures/aile_rouge.jpg");

    /* Initialisations */
    wing = new Wing(1, tex_corne, wing_R, wing_G, wing_B, tex_aile, tex_peau);
    patte = new Patte(0.8, tex_peau, skin_R, skin_G, skin_B, tex_corne);
    tete = new Tete(1.5, tex_corne, skin_R, skin_G, skin_B, tex_peau);
    tail = new Tail(0.6, tex_peau, skin_R, skin_G, skin_B);
    body = new Body(tex_peau, skin_R, skin_G, skin_B);
    neck = new Neck(1.4, 1.0, 0.8, 3, 0.6, tex_peau, skin_R, skin_G, skin_B);
    leg = new Leg(0.6, tex_peau, skin_R, skin_G, skin_B);
    feu = new Feu(1);

    leash = new Leash(gravity, mediumViscosity, dt);
}

void Landscape::animate() {
    if (this->cycle == 1000) {
        this->cycle = 0;
        feu->reinit();
    }
    this->cycle += 1;
    this->angle += 0.01;
    leash->animateLeash();
    wing->animateWing(72, 2);
    tail->animateTail();
    feu->animateFeu();
    if (this->hauteur >= 1.8 or this->hauteur <= -1.8) {
        this->sens = -this->sens;
        this->hauteur += this->sens;
    } else {
        this->hauteur += this->sens;
    }
}

void Landscape::keyPressEvent(QKeyEvent* e, Viewer& viewer) {
    // Get event modifiers key
    const Qt::KeyboardModifiers modifiers = e->modifiers();

    if ((e->key() == Qt::Key_Home) && (modifiers == Qt::NoButton)) {
        // stop the animation, and reinit the scene
        //viewer.stopAnimation();
        init(viewer);
    }
}

void Landscape::drawLandscape() {
    /* Même couleur que le brouillard */
    glColor3f(0.8, 0.8, 0.8);

    glPushMatrix();

    /* Initialisation de la matrice unité */
    glLoadIdentity();

    /* Paramétrage */
    glDisable(GL_DEPTH_TEST); // Fond toujours affiché derrière les objets
    glDisable(GL_LIGHTING); // Pas de lumière sur le fond

    glEnable(GL_TEXTURE_2D);

    glBindTexture(GL_TEXTURE_2D, tex_landscape);
    glBegin(GL_QUADS);
    glTexCoord2f(0, 0);
    glVertex3f(0.5, -0.5, -0.5);
    glTexCoord2f(1, 0);
    glVertex3f(-0.5, -0.5, -0.5);
    glTexCoord2f(1, 1);
    glVertex3f(-0.5, 0.5, -0.5);
    glTexCoord2f(0, 1);
    glVertex3f(0.5, 0.5, -0.5);
    glEnd();

    glDisable(GL_TEXTURE_2D);

    /* Remise en ordre des paramètres */
    glEnable(GL_LIGHTING);
    glEnable(GL_DEPTH_TEST);

    glPopMatrix();

    glColor3f(1, 1, 1);
}
