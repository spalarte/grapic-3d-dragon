#include <iostream>
#include <numeric>
using namespace std;
#include "math.h"
#include "corne.h"

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

Corne::Corne(float rayon1, float rayon2, float rayon3, float angle1, float angle2,
        GLuint texture) {

    this->rayon1 = rayon1;
    this->rayon2 = rayon2;
    this->rayon3 = rayon3;
    this->angle1 = angle1;
    this->angle2 = angle2;
    init();

    /* Initialisation de la texture */
    tex_corne = texture;
    init_textures();
}

void Corne::init() {

    for (int i = 0; i<6; i++) {
        for (int j = 0; j<11; j++) {
            ver_c[i][j][0] = (rayon2 + cos(2*M_PI/10*j) * rayon1 / (i+1)) * cos(i*angle1) - rayon2;
            ver_c[i][j][1] = (rayon2 + cos(2*M_PI/10*j) * rayon1 / (i+1)) * sin(i*angle1);
            ver_c[i][j][2] = rayon1 * sin(2*M_PI/10*j) / (i + 1);
        }
    }

    for (int i = 0; i<6; i++) {
        for (int j = 0; j<11; j++) {
            nor_c[i][j][0] = cos(2*M_PI/10*j) * cos(i*angle1);
            nor_c[i][j][1] = cos(2*M_PI/10*j) * sin(i*angle1);
            nor_c[i][j][2] = sin(2*M_PI/10*j);
        }
    }

    for (int i = 0; i<6; i++) {
        for (int j = 0; j<11; j++) {
            ver_c[i + 6][j][0] = (rayon3 + cos(2*M_PI/10*j) * rayon1 / (i + 6)) * cos(-i*angle2) - rayon3;
            ver_c[i + 6][j][1] = (rayon3 + cos(2*M_PI/10*j) * rayon1 / (i + 6)) * sin(-i*angle2);
            ver_c[i + 6][j][2] = rayon1 * sin(2*M_PI/10*j) / (i + 6);
        }
    }

    for (int i = 0; i<6; i++) {
        for (int j = 0; j<11; j++) {
            nor_c[i + 6][j][0] = cos(2*M_PI/10*j) * cos(-i*angle2);
            nor_c[i + 6][j][1] = cos(2*M_PI/10*j) * sin(-i*angle2);
            nor_c[i + 6][j][2] = sin(2*M_PI/10*j);
        }
    }

}

void Corne::init_textures() {
    /* On considère la corne comme un cone */
    GLfloat rayon = ver_c[0][0][0];
    /* d² = r² + 1² */
    GLfloat d = sqrt(rayon * rayon + 1);
    /* longueur arc propotionnel angle au centre */
    GLfloat p = 2 * M_PI * rayon;
    GLfloat angle_centre_total = (360 * p) / (2 * M_PI * d);    //degrés
    GLfloat angle_tex = (angle_centre_total / 11) * (M_PI / 180);

    GLfloat angle_cour = 0;
    GLfloat rayon_cour = rayon * 2;

    for(GLint i = 0; i < 12; i++) {
        for(GLint j = 0; j < 11; j++) {
            textures[i][j][0] = cos(angle_cour) * rayon_cour;
            textures[i][j][1] = sin(angle_cour) * rayon_cour;

            angle_cour += angle_tex;
        }
        rayon_cour = ver_c[i + 1][0][0] * 2;
    }
}

void Corne::draw() {

    glEnable(GL_TEXTURE_2D);

    /* On bind la texture (style marbre) */
    glBindTexture(GL_TEXTURE_2D, tex_corne);
    /* Répétition sur les x */
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    /* Répétition sur les y */
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    glPushMatrix();

    glBegin(GL_TRIANGLE_STRIP);
    for (int i = 0; i<5; i++) {
        for (int j = 0; j<11; j++) {
            glTexCoord2fv(textures[i][j]);
            glNormal3fv(nor_c[i][j]);
            glVertex3fv(ver_c[i][j]);
            
            glTexCoord2fv(textures[i + 1][j]);
            glNormal3fv(nor_c[i + 1][j]);
            glVertex3fv(ver_c[i + 1][j]);
        }
    }

    glEnd();

    glPushMatrix();
    glTranslatef(-rayon2, 0, 0);
    glRotatef(180, 0, 0, 1);
    glRotatef((int)(5*angle1*360.0/(2.0*M_PI)), 0, 0, 1);
    glTranslatef(-rayon2, 0, 0);

    glBegin(GL_TRIANGLE_STRIP);
    for (int i = 6; i<11; i++) {
        for (int j = 0; j<11; j++) {
            glTexCoord2fv(textures[i][j]);
            glNormal3fv(nor_c[i][j]);
            glVertex3fv(ver_c[i][j]);
            
            glTexCoord2fv(textures[i + 1][j]);
            glNormal3fv(nor_c[i + 1][j]);
            glVertex3fv(ver_c[i + 1][j]);
        }
    }

    glEnd();

    glPopMatrix();

    glPopMatrix();

    glDisable(GL_TEXTURE_2D);
}
