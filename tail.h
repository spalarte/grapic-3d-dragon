#ifndef _TAIL_
#define _TAIL_

#include "renderable.h"
#ifndef __APPLE__
#include <GL/glut.h>
#else
#include <GLUT/glut.h>
#endif

#define NB_VERTEX 20
#define NB_CERCLES 10

class Tail : public Renderable {
    public:
        Tail(float taille, GLuint texture, GLfloat r, GLfloat g, GLfloat b);
        ~Tail();
        void draw();
        void animateTail();
    private:
        float taille;
        /* Angle entre deux points d'un cercle */
        GLfloat angle;
        /* Points qui composent la queue : n points * 3 coordonnées par cercles * m cercles */
        GLfloat vertices_elem[NB_VERTEX * 3 * NB_CERCLES];
        /* Normales */
        GLfloat normals_elem[NB_VERTEX * 3 * NB_CERCLES];
        /* Coordonnées de la texture */
        GLfloat textures_elem[NB_VERTEX * 2 * NB_CERCLES];

        /* Indices des faces : n points par cercle + 1 pour fermer le cercle * m cercles */
        typedef struct index {
                index() {
                    next = NULL;
                }
                GLubyte elem[(NB_VERTEX +1) * 2];
                struct index *next;
        } index_t;
        index_t *indices;

        /* Position du cone terminant la queue */
        GLfloat cone_x, cone_z, cone_r;

        /* Coordonnées de la texture pour le cône de fin */
        GLfloat textures_cone[NB_VERTEX * 2];

        /* Texture */
        GLuint tex_peau;

        /* Couleur */
        GLfloat color_R;
        GLfloat color_G;
        GLfloat color_B;

        /* Animation */
        float angle2;
        int sens;

        void init_cercle(GLfloat r, GLint index, GLfloat z, GLfloat dx, GLfloat tex_x);
        void init_indices(GLint cercle1, GLint cercle2, index_t *id);
        void init_cone();

        void draw_pointe();
};

#endif
