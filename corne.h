#ifndef _CORNE_
#define _CORNE_

#include "renderable.h"
#ifndef __APPLE__
#include <GL/glut.h>
#else
#include <GLUT/glut.h>
#endif

class Corne : public Renderable {
public:
    void init();
    void draw();
    Corne(float rayon1, float rayon2, float rayon3, float angle1, float angle2,
            GLuint texture);

private:
    float rayon1;
    float rayon2;
    float rayon3;
    float angle1;
    float angle2;
    GLfloat ver_c[12][11][3];
    GLfloat nor_c[12][11][3];

    /* Texture */
    GLuint tex_corne;
    GLfloat textures[12][11][2];
    void init_textures();
};

#endif
