#include <iostream>
#include <numeric>
using namespace std;
#include "tete.h"

#define PI 3.141592

Tete::Tete(float taille, GLuint texture_cornes, GLfloat r, GLfloat g, GLfloat b,
        GLuint texture_peau) {

	this->taille = taille;

    // Initialisation des cornes
	c1 = new Corne(0.4, 1, 0.7, 2.0*PI/25.0, 2.0*PI/30.0, texture_cornes);
	c2 = new Corne(0.3, 0.7, 0.5, 2.0*PI/25.0, 2.0*PI/30.0, texture_cornes);
	c3 = new Corne(0.3, 0.5, 0.3, 2.0*PI/30.0, 2.0*PI/50.0, texture_cornes);

	init();

    /* Couleur */
    color_R = r;
    color_G = g;
    color_B = b;

    /* Texture */
    tex_peau = texture_peau;
}

Tete::~Tete() {
    delete c1;
    delete c2;
    delete c3;
}

void Tete::init() {
	float y1 = 0.4;
	float y2 = 0.2;
	float x1 = 0.8;
	float x2 = 2;
	float dx = x2 - x1;
	float dy = y1 - y2;

	ver_t[0][0] = x1;
	ver_t[0][1] = -y1;
	ver_t[0][2] = -y1;
	ver_t[1][0] = x2;
	ver_t[1][1] = - y2;
	ver_t[1][2] = - y2;
	ver_t[2][0] = x1;
	ver_t[2][1] = y1;
	ver_t[2][2] = -y1;
	ver_t[3][0] = x2;
	ver_t[3][1] = y2;
	ver_t[3][2] = -y2;
	ver_t[4][0] = x1;
	ver_t[4][1] = y1;
	ver_t[4][2] = y1;
	ver_t[5][0] = x2;
	ver_t[5][1] = y2;
	ver_t[5][2] = y2;
	ver_t[6][0] = x1;
	ver_t[6][1] = -y1;
	ver_t[6][2] = y1;
	ver_t[7][0] = x2;
	ver_t[7][1] = -y2;
	ver_t[7][2] = y2;

	nor_t[0][0] = 2 * dy;
	nor_t[0][1] = -dx;
	nor_t[0][2] = -dx;
	nor_t[1][0] = 2 * dy;
	nor_t[1][1] = dx;
	nor_t[1][2] = -dx;
	nor_t[2][0] = 2 * dy;
	nor_t[2][1] = dx;
	nor_t[2][2] = dx;
	nor_t[3][0] = 2 * dy;
	nor_t[3][1] = -dx;
	nor_t[3][2] = dx;

	nor_t[4][0] = 1;
	nor_t[4][1] = -1;
	nor_t[4][2] = -1;
	nor_t[5][0] = 1;
	nor_t[5][1] = 1;
	nor_t[5][2] = -1;
	nor_t[6][0] = 1;
	nor_t[6][1] = 1;
	nor_t[6][2] = 1;
	nor_t[7][0] = 1;
	nor_t[7][1] = -1;
	nor_t[7][2] = 1;
}

void Tete::draw() {

    /* Couleur appliquée */
    glColor3f(color_R, color_G, color_B);
    glEnable(GL_TEXTURE_2D);

    /* On bind la texture */
    glBindTexture(GL_TEXTURE_2D, tex_peau);
    /* Répétition sur les x */
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    /* Pas de répétition sur les y */
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

	glPushMatrix();

	glScalef(taille, taille, taille);

    /* Texture à plaquer sur la sphère */
    GLUquadric *params = gluNewQuadric();
    gluQuadricTexture(params, GL_TRUE);
    gluQuadricDrawStyle(params, GL_FILL);

    /* Dessin de la tête */
	gluSphere(params, 1, 20, 20);

    /* Variables courantes des coordonnées de textures */
    GLfloat tex_x = 0;
    GLint tex_y = 0;

    /* Dessin du museau (longueur) */
	glBegin(GL_QUAD_STRIP);

        glTexCoord2d(tex_x, tex_y);
	    glNormal3fv(nor_t[0]);
        glVertex3fv(ver_t[0]);
        tex_y = tex_y ^ 1;

        glTexCoord2d(tex_x, tex_y);
        glNormal3fv(nor_t[4]);
        glVertex3fv(ver_t[1]);
        tex_x += 0.5; tex_y = tex_y ^ 1;

        glTexCoord2d(tex_x, tex_y);
        glNormal3fv(nor_t[1]);
        glVertex3fv(ver_t[2]);
        tex_y = tex_y ^ 1;

        glTexCoord2d(tex_x, tex_y);
        glNormal3fv(nor_t[5]);
        glVertex3fv(ver_t[3]);
        tex_x += 0.5; tex_y = tex_y ^ 1;

        glTexCoord2d(tex_x, tex_y);
        glNormal3fv(nor_t[2]);
        glVertex3fv(ver_t[4]);
        tex_y = tex_y ^ 1;

        glTexCoord2d(tex_x, tex_y);
        glNormal3fv(nor_t[6]);
        glVertex3fv(ver_t[5]);
        tex_x += 0.5; tex_y = tex_y ^ 1;

        glTexCoord2d(tex_x, tex_y);
        glNormal3fv(nor_t[3]);
        glVertex3fv(ver_t[6]);
        tex_y = tex_y ^ 1;

        glTexCoord2d(tex_x, tex_y);
        glNormal3fv(nor_t[7]);
        glVertex3fv(ver_t[7]);
        tex_x += 0.5; tex_y = tex_y ^ 1;

        glTexCoord2d(tex_x, tex_y);
        glNormal3fv(nor_t[0]);
        glVertex3fv(ver_t[0]);
        tex_y = tex_y ^ 1;

        glTexCoord2d(tex_x, tex_y);
        glNormal3fv(nor_t[4]);
        glVertex3fv(ver_t[1]);

	glEnd();

    /* Dessin du museau (avant) */
	glBegin(GL_POLYGON);

        glTexCoord2d(0, 0);
    	glNormal3fv(nor_t[4]);
        glVertex3fv(ver_t[1]);

        glTexCoord2d(1, 0);
        glNormal3fv(nor_t[5]);
        glVertex3fv(ver_t[3]);

        glTexCoord2d(1, 1);
        glNormal3fv(nor_t[6]);
        glVertex3fv(ver_t[5]);

        glTexCoord2d(0, 1);
        glNormal3fv(nor_t[7]);
        glVertex3fv(ver_t[7]);
        
    glEnd();

    /* Pas de couleur ni de texture lors du dessin des détails (cornes/yeux) */
    glDisable(GL_TEXTURE_2D);
    glColor3f(1, 1, 1);

    // Cornes de taille 1
	glPushMatrix();
	glRotatef(-30, 1, 0, 0);
	glTranslatef(0, 0, 0.9);
	glRotatef(90, 1, 0, 0);
	c1->draw();
	glPopMatrix();
	glPushMatrix();
	glRotatef(30, 1, 0, 0);
	glTranslatef(0, 0, 0.9);
	glRotatef(90, 1, 0, 0);
	c1->draw();
	glPopMatrix();

    // Cornes de taille 2
	glPushMatrix();
	glRotatef(-50, 1, 0, 0);
	glTranslatef(0, 0, 0.9);
	glRotatef(90, 1, 0, 0);
	c2->draw();
	glPopMatrix();
	glPushMatrix();
	glRotatef(50, 1, 0, 0);
	glTranslatef(0, 0, 0.9);
	glRotatef(90, 1, 0, 0);
	c2->draw();
	glPopMatrix();

    // Cornes de taille 3
	glPushMatrix();
	glRotatef(-150, 1, 0, 0);
	glRotatef(40, 0, 1, 0);
	glTranslatef(0, 0, 0.9);
	glRotatef(90, 1, 0, 0);
	c3->draw();
	glPopMatrix();
	glPushMatrix();
	glRotatef(150, 1, 0, 0);
	glRotatef(40, 0, 1, 0);
	glTranslatef(0, 0, 0.9);
	glRotatef(90, 1, 0, 0);
	c3->draw();
	glPopMatrix();

    /* Dessin des yeux */
    glColor3f(1, 0, 0);
	glPushMatrix();
	glRotatef(-50, 1, 0, 0);
	glRotatef(40, 0, 1, 0);
	glTranslatef(0, 0, 1);
	glRotatef(90, 1, 0, 0);
	glScalef(0.2, 0.1, 0.1);
	glutSolidOctahedron();
	glPopMatrix();
	glPushMatrix();
	glRotatef(50, 1, 0, 0);
	glRotatef(40, 0, 1, 0);
	glTranslatef(0, 0, 1);
	glRotatef(90, 1, 0, 0);
	glScalef(0.2, 0.1, 0.1);
	glutSolidOctahedron();
	glPopMatrix();
    glColor3f(1, 1, 1);

    /* Dessin de la petite corne sur le nez */
	glPushMatrix();
	glTranslatef(1.6, 0, 0.2);
	glScalef(0.2, 0.2, 0.15);
    glColor3f(0, 0, 0);
	glutSolidOctahedron();
    glColor3f(1, 1, 1);
	glPopMatrix();	

	glPopMatrix();
}
