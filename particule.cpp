#include <iostream>
#include <numeric>
using namespace std;
#include "particule.h"

Particule::Particule() {
	reinit();
}

void Particule::reinit() {
	this->active = true;  
    
        this->life = 1.0;  

	this->life_cycle = 0;

        this->fade = myRandom(0.01, 0.05);

        this->r = myRandom(200.0, 255.0);  
        this->g = myRandom(0.0, 1.0);  
        this->b = 0; 

	this->x = 0.0;
	this->y = 0.0;
	this->z = 0.0;

        this->xi = myRandom(-10.0, 10.0);
        this->yi = myRandom(-10.0, 10.0);
        this->zi = myRandom(20.0, 30.0);

        this->xg = 0.0;           
        this->yg = 0.0;
        this->zg = 1.0;
}

void Particule::draw() {
	glPushMatrix();
	if (this->active) {
		glColor4d(this->r, this->g, this->b, this->life);
		glTranslatef(this->x, this->y, this->z);
		glutSolidSphere(0.05, 10, 10);
	}
	glPopMatrix();
}

void Particule::animateParticule() {
	if (this->life_cycle == 5) {
		this->active = false;
	}

	this->x += this->xi/1000;
	this->y += this->yi/1000;
	this->z += this->zi/1000;

	this->xi += this->xg;
	this->yi += this->yg;
	this->zi += this->zg;

	this->life -= this->fade;

	if (this->life < 0.0) {
		this->life = 1.0;    

		this->life_cycle += 1;

		this->fade = myRandom(0.01, 0.05);

		this->x = 0.0;
		this->y = 0.0;
		this->z = 0.0;

		this->xi = myRandom(-10.0, 10.0);   
		this->yi = myRandom(-10.0, 10.0);
		this->zi = myRandom(20.0, 30.0);

		this->r = myRandom(200.0, 255.0);  
		this->g = myRandom(0.0, 1.0); 
	}
}

float Particule::myRandom(float min, float max) {
	return min + ((float)rand() / (float)RAND_MAX) * (max - min);
}


