#include "textures_util.h"
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>

#include <iostream>
#include <cstdio>
#include <cstring>
#include <cstdlib>

SDL_Surface * flipSurface(SDL_Surface * surface);

/*****************************************************************
 * ***** Utilisation de SDL_Image pour charger une texture ***** *
 *****************************************************************/
/* source : http://openclassrooms.com/courses/creez-des-programmes-en-3d-avec-opengl/les-textures-3 */

GLuint loadTexture(const char * filename,bool useMipMap)
{
    GLuint glID;
    SDL_Surface * picture_surface = NULL;
    SDL_Surface *gl_surface = NULL;
    SDL_Surface * gl_fliped_surface = NULL;
    Uint32 rmask, gmask, bmask, amask;

    picture_surface = IMG_Load(filename);
    if (picture_surface == NULL)
        return 0;

#if SDL_BYTEORDER == SDL_BIG_ENDIAN

    rmask = 0xff000000;
    gmask = 0x00ff0000;
    bmask = 0x0000ff00;
    amask = 0x000000ff;
#else

    rmask = 0x000000ff;
    gmask = 0x0000ff00;
    bmask = 0x00ff0000;
    amask = 0xff000000;
#endif

    SDL_PixelFormat format = *(picture_surface->format);
    format.BitsPerPixel = 32;
    format.BytesPerPixel = 4;
    format.Rmask = rmask;
    format.Gmask = gmask;
    format.Bmask = bmask;
    format.Amask = amask;

    gl_surface = SDL_ConvertSurface(picture_surface,&format,SDL_SWSURFACE);

    gl_fliped_surface = flipSurface(gl_surface);

    glGenTextures(1, &glID);

    glBindTexture(GL_TEXTURE_2D, glID);


    if (useMipMap)
    {

        gluBuild2DMipmaps(GL_TEXTURE_2D, 4, gl_fliped_surface->w,
                gl_fliped_surface->h, GL_RGBA,GL_UNSIGNED_BYTE,
                gl_fliped_surface->pixels);

        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,
                GL_LINEAR_MIPMAP_LINEAR);

    }
    else
    {
        glTexImage2D(GL_TEXTURE_2D, 0, 4, gl_fliped_surface->w,
                gl_fliped_surface->h, 0, GL_RGBA,GL_UNSIGNED_BYTE,
                gl_fliped_surface->pixels);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    }
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);


    SDL_FreeSurface(gl_fliped_surface);
    SDL_FreeSurface(gl_surface);
    SDL_FreeSurface(picture_surface);

    return glID;
}

SDL_Surface * flipSurface(SDL_Surface * surface)
{
    int current_line,pitch;
    SDL_Surface * fliped_surface = SDL_CreateRGBSurface(SDL_SWSURFACE,
            surface->w,surface->h,
            surface->format->BitsPerPixel,
            surface->format->Rmask,
            surface->format->Gmask,
            surface->format->Bmask,
            surface->format->Amask);
    
    SDL_LockSurface(surface);
    SDL_LockSurface(fliped_surface);
    
    pitch = surface->pitch;
    for (current_line = 0; current_line < surface->h; current_line ++)
    {
        memcpy(&((unsigned char* )fliped_surface->pixels)[current_line*pitch],
                &((unsigned char* )surface->pixels)[(surface->h - 1  - current_line)*pitch],
                pitch);
    }
    
    SDL_UnlockSurface(fliped_surface);
    SDL_UnlockSurface(surface);
    return fliped_surface;
}

/* Retourne une surface SDL correspondant à l'image */
SDL_Surface * loadImageCubeMap(const char *filename)
{
    SDL_Surface * picture_surface = NULL;
    SDL_Surface *gl_surface = NULL;
    SDL_Surface * gl_fliped_surface = NULL;
    Uint32 rmask, gmask, bmask, amask;

    picture_surface = IMG_Load(filename);
    if (picture_surface == NULL)
        return 0;

#if SDL_BYTEORDER == SDL_BIG_ENDIAN

    rmask = 0xff000000;
    gmask = 0x00ff0000;
    bmask = 0x0000ff00;
    amask = 0x000000ff;
#else

    rmask = 0x000000ff;
    gmask = 0x0000ff00;
    bmask = 0x00ff0000;
    amask = 0xff000000;
#endif

    SDL_PixelFormat format = *(picture_surface->format);
    format.BitsPerPixel = 32;
    format.BytesPerPixel = 4;
    format.Rmask = rmask;
    format.Gmask = gmask;
    format.Bmask = bmask;
    format.Amask = amask;

    gl_surface = SDL_ConvertSurface(picture_surface,&format,SDL_SWSURFACE);

    gl_fliped_surface = flipSurface(gl_surface);

    SDL_FreeSurface(gl_surface);
    SDL_FreeSurface(picture_surface);

    return gl_fliped_surface;
}

/* Création d'une texture de type CubeMap */
GLuint loadCubeMapTexture(const char *top, const char *bottom, const char *left,
        const char *front, const char *right, const char *back)
{
    /* Identifiant de la texture "CubeMap" */
    GLuint textureID;
    glGenTextures(1, &textureID);
    glActiveTexture(GL_TEXTURE0);

    /* On bind la texture de type CubeMap */
    glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);

    /* On associe les chaque face à la texture */

    /* 1) Top */
    SDL_Surface *top_surface = loadImageCubeMap(top);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, 4, top_surface->w,
            top_surface->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, top_surface->pixels);

    /* 2) Bottom */
    SDL_Surface *bottom_surface = loadImageCubeMap(bottom);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, 4, bottom_surface->w,
            bottom_surface->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, bottom_surface->pixels);

    /* 3) Front */
    SDL_Surface *front_surface = loadImageCubeMap(front);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, 4, front_surface->w,
            front_surface->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, front_surface->pixels);

    /* 4) Back */
    SDL_Surface *back_surface = loadImageCubeMap(back);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, 4, back_surface->w,
            back_surface->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, back_surface->pixels);

    /* 5) Left */
    SDL_Surface *left_surface = loadImageCubeMap(left);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, 4, left_surface->w,
            left_surface->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, left_surface->pixels);

    /* 6) Right */
    SDL_Surface *right_surface = loadImageCubeMap(right);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, 4, right_surface->w,
            right_surface->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, right_surface->pixels);

    /* Paramètres */
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

    /* On libère les objets utilisés */
    SDL_FreeSurface(top_surface);
    SDL_FreeSurface(bottom_surface);
    SDL_FreeSurface(front_surface);
    SDL_FreeSurface(back_surface);
    SDL_FreeSurface(left_surface);
    SDL_FreeSurface(right_surface);

    /* On "dé-bind" */
    glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

    return textureID;
}
