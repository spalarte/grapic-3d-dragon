#include <iostream>
#include <numeric>
using namespace std;
#include "math.h"
#include "griffe.h"

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

Griffe::Griffe(float rayon1, float rayon2, float angle, GLuint texture) {
    this->rayon1 = rayon1;
    this->rayon2 = rayon2;
    this->angle = angle;
    init();
    init_textures();

    /* Texture qui va être appliquée */
    tex_griffe = texture;
}

void Griffe::init() {

    for (int i = 0; i<6; i++) {
        for (int j = 0; j<11; j++) {
            ver_g[i][j][0] = (rayon2 + cos(2*M_PI/10*j) * rayon1 / (float)pow(2,i)) * cos(i*angle) - rayon2;
            ver_g[i][j][1] = (rayon2 + cos(2*M_PI/10*j) * rayon1 / (float)pow(2,i)) * sin(i*angle);
            ver_g[i][j][2] = rayon1 * sin(2*M_PI/10*j) / (float)pow(2,i);
        }
    }

    for (int i = 0; i<6; i++) {
        for (int j = 0; j<11; j++) {
            nor_g[i][j][0] = cos(2*M_PI/10*j) * cos(i*angle);
            nor_g[i][j][1] = cos(2*M_PI/10*j) * sin(i*angle);
            nor_g[i][j][2] = sin(2*M_PI/10*j);
        }
    }

}

void Griffe::init_textures() {
    /* On considère la griffe comme un cone */
    GLfloat rayon = ver_g[0][2][2];
    /* d² = r² + 1² */
    GLfloat d = sqrt(rayon * rayon + 1);
    /* longueur arc proportionnel angle au centre */
    GLfloat p = 2 * M_PI * rayon;
    GLfloat angle_centre_total = (360 * p) / (2 * M_PI * d); // degrés
    GLfloat angle_tex = (angle_centre_total/11) * (M_PI/180);

    GLfloat angle_cour = 0;
    GLfloat rayon_cour = rayon * 10;

    for(GLint i = 0; i < 6; i++) {
        for(GLint j = 0; j < 11; j++) {
            textures[i][j][0] = cos(angle_cour) * rayon_cour;
            textures[i][j][1] = sin(angle_cour) * rayon_cour;

            angle_cour += angle_tex;
        }
        rayon_cour = ver_g[i + 1][2][2] * 10;
    }
}

void Griffe::draw() {

    glEnable(GL_TEXTURE_2D);

    /* On bind la texture (style marbre) */
    glBindTexture(GL_TEXTURE_2D, tex_griffe);
    /* Répétition sur les x */
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    /* Répétition sur les y */
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    glPushMatrix();

    glBegin(GL_TRIANGLE_STRIP);
    for (int i = 0; i<5; i++) {
        for (int j = 0; j<11; j++) {
            glTexCoord2fv(textures[i][j]);
            glNormal3fv(nor_g[i][j]);
            glVertex3fv(ver_g[i][j]);

            glTexCoord2fv(textures[i + 1][j]);
            glNormal3fv(nor_g[i + 1][j]);
            glVertex3fv(ver_g[i + 1][j]);
        }
    }
    glEnd();

    glPopMatrix();

    glDisable(GL_TEXTURE_2D);
}
