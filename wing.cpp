#include <iostream>
using namespace std;
#include "wing.h"
#include <cmath>

#define MI_AILE 4.5
#define LONGUEUR_AILE 10
#define X_P2 2
#define Y_P2 3
#define X_P3 6
#define Y_P3 5
#define X_P5 12
#define Y_P5 4.5

#define RATIO_ARETE 1.1

#define PI 3.141592

Wing::Wing(float ratio, GLuint tex_griffe, GLfloat r, GLfloat g, GLfloat b,
        GLuint texture_ailes, GLuint texture_cones) {

    this->ratio = ratio;
    this->Z_ANGLE = this->compteur = 0;
    this->direction = false;
    //on remplit les coordonnées de l'aile
    fillTable();

    griffe = new Griffe(0.2, 0.8, 2.0*PI/25.0, tex_griffe);

    /* Couleurs de l'aile */
    color_R = r;
    color_G = g;
    color_B = b;

    /* Texture */
    tex_aile = texture_ailes;
    tex_cones = texture_cones;
}

void Wing::draw() {

    glPushMatrix();

    glRotatef(Z_ANGLE, 0, 1, 0); //pour animation des ailes

    //on affiche l'aile "plate"
    drawImmediate();

    //cônes sur les ailes
    glColor3f(color_R, color_G, color_B);
    drawCones();

    glPopMatrix();

    glColor3f(1, 1, 1);
}

void Wing::drawCones() {

    glEnable(GL_TEXTURE_2D);

    /* On bind la texture */
    glBindTexture(GL_TEXTURE_2D, tex_cones);

    /* Texture à plaquer sur un cone */
    GLUquadric *params = gluNewQuadric();
    gluQuadricDrawStyle(params, GL_FILL);
    gluQuadricTexture(params, GL_TRUE);

    //arête principale de l'aile

    glRotatef(90, 0, 1, 0);
    gluCylinder(params, ratio / 5, 0, LONGUEUR_AILE * ratio * RATIO_ARETE, 20, 20);

    glTranslatef(0, 0, MI_AILE * ratio);
    glPushMatrix();
    glRotatef(90, 0, 1, 0);
    //pas de couleur pour la griffe
    glColor3f(1, 1, 1);
    griffe->draw();
    glPopMatrix();

    /* On rétablit la bonne texture */
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, tex_cones);

    /* couleur des arrêtes internes de l'aile */
    glColor3f(1, 0, 0);

    //cone 1
    glRotatef(atan((Y_P5 / (X_P5 - MI_AILE)))* 180 / M_PI, 1, 0, 0);
    gluCylinder(params, ratio / 5, 0,
            sqrt(((X_P5 - MI_AILE)*(X_P5 - MI_AILE)) + Y_P5 * Y_P5)
            * ratio * RATIO_ARETE, 20, 20);

    //cone 2
    glRotatef(atan((Y_P5 / (X_P5 - MI_AILE)))* 180 / M_PI, -1, 0, 0); //reset pour faciliter calcul
    glRotatef(atan((Y_P3 / (X_P3 - MI_AILE)))* 180 / M_PI, 1, 0, 0);
    gluCylinder(params, ratio / 5, 0,
            sqrt(((X_P3 - MI_AILE)*(X_P3 - MI_AILE)) + Y_P3 * Y_P3)
            * ratio * RATIO_ARETE, 20, 20);

    //cone 3
    glRotatef(atan((Y_P3 / (X_P3 - MI_AILE)))* 180 / M_PI, -1, 0, 0); //reset
    glRotatef(180 - atan((Y_P2 / (MI_AILE - X_P2)))* 180 / M_PI, 1, 0, 0);
    gluCylinder(params, ratio / 5, 0,
            sqrt(((X_P2 - MI_AILE)*(X_P2 - MI_AILE)) + Y_P2 * Y_P2)
            * ratio * RATIO_ARETE, 20, 20);

    /* Suppression des paramètres */
    gluDeleteQuadric(params);

    glDisable(GL_TEXTURE_2D);
}

void Wing::fillTable() { //c++11 ? si oui, plus jolie pour remplir ... {x, y, z}
    ver_wing[0][0] = MI_AILE * ratio;
    ver_wing[0][1] = 0;
    ver_wing[1][0] = 0;
    ver_wing[1][1] = 0;
    ver_wing[2][0] = X_P2 * ratio;
    ver_wing[2][1] = -Y_P2 * ratio;
    ver_wing[3][0] = X_P3 * ratio;
    ver_wing[3][1] = -Y_P3 * ratio;
    ver_wing[4][0] = 9 * ratio;
    ver_wing[4][1] = -4 * ratio;
    ver_wing[5][0] = X_P5 * ratio;
    ver_wing[5][1] = -Y_P5 * ratio;
    ver_wing[6][0] = 9.5 * ratio;
    ver_wing[6][1] = -2 * ratio;
    ver_wing[7][0] = 9.5 * ratio;
    ver_wing[7][1] = -1 * ratio;
    ver_wing[8][0] = LONGUEUR_AILE * ratio;
    ver_wing[8][1] = 0;

    for (int i = 0; i < 9; i++) {

        ver_wing[i][2] = 0;
    }
}

void Wing::drawImmediate() {
    // show both faces
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    glEnable(GL_TEXTURE_2D);

    /* On bind la texture */
    glBindTexture(GL_TEXTURE_2D, tex_aile);
    /* Répétition sur les x */
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    /* Répétition sur les y */
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    glBegin(GL_TRIANGLE_FAN);
    glNormal3f(0.0, 0.0, -1.0);
    for (int i = 0; i < 9; i++) {
        glTexCoord2d(ver_wing[i][0]/5.0, ver_wing[i][1]/5.0);
        glVertex3fv(ver_wing[i]);
    }
    glEnd();

    glDisable(GL_TEXTURE_2D);
}

void Wing::animateWing(int amplitude, float vitesse) { //Test
    animateWingDirection(vitesse, direction);
    if (compteur >= amplitude / vitesse) {
        direction = !direction;
        compteur = 0;
    }
    compteur++;

}

void Wing::animateWingDirection(float vitesse, bool direction) {
    if (direction) {
        Z_ANGLE += vitesse;
    } else {
        Z_ANGLE -= vitesse;
    }

}
