*****************************
***                       ***
***     Graphique 3D :    ***
***                       ***
***     My pet dragon     ***
***                       ***
*****************************


Ensimag - 27 avril 2015


* Nicolas Brengard
* Elise Klingelschmidt
* Emma Spalart


*****************************
***     Organisation      ***
*****************************
Les sources (*.h et *.cpp) de notre projet se situent à la racine de celui-ci.

La racine comprend aussi le Makefile (à générer, voir section "Lancement")
ainsi que l'exécutable.

Le répertoire "doc" comprend le sujet de ce TP.

Le répertoire "illustrations" comprend l'illustration de notre dragon :
- l'image qui nous a inspiré le dragon
- la vidéo que nous avons réalisé

Le répertoire "textures" comprend les textures que nous utilisons.


*****************************
***       Lancement       ***
*****************************
1)  Créer le Makefile suivant la configuration de votre machine :
    qmake-qt4 cg3D.pro

    Remarque : Utilisez le fichier cg3D.pro fourni dans notre projet. En
    effet, notre projet utilise les options de compilation SDL non présentes
    dans le fichier cg3D.pro d'origine.

2)  Compilez les sources :
    make

3)  Lancez l'exécutable :
    ./cg3D

4)  Lancez les animations :
    Bouton "Entrée" du clavier


*****************************
***   Contenu du projet   ***
*****************************
1)  Dragon :
    Le dragon a été assemblé à partir d'éléments du corps (aile, patte,
    ...) que nous avons créés séparément. Le dragon est assemblé morceau
    par morceau dans la fonction "drawDragon()" de la classe Landscape.

2)  Textures et couleurs :
    Chaque partie du dragon a été texturisée. Le dragon est donc
    entièrement recouvert de textures. Certaines parties du dragon ont
    également été colorées.

3)  Animations basiques :
    Les ailes du dragon sont animées pour le faire voler. Sa queue est
    également animée pour produire un mouvement de haut en bas.

4)  Chaine :
    Notre dragon est attaché par le cou à une chaine qui bouge grâce à un
    système masse-ressort.

5)  Feu :
    Notre dragon crache également du feu. Nous avons choisi de représenter le
    feu par un système de particules animées.

6)  Décor :
    Le dragon vole dans un ciel orageux (texture de fond) avec du brouillard.
