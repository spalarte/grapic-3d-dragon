#include <iostream>
#include <numeric>
using namespace std;
#include "patte.h"

#define PI 3.141592

Patte::Patte(float taille, GLuint texture, GLfloat r, GLfloat g,
        GLfloat b, GLuint tex_griffe) {

	this->taille = taille;
	
    griffe = new Griffe(0.1, 0.5, 2.0*PI/25.0, tex_griffe);

	init();

    /* Chargement de la texture */
    tex_peau = texture;

    /* Couleur de la peau (devra être "désactivée" pour dessiner les griffes) */
    color_R = r;
    color_G = g;
    color_B = b;
}

Patte::~Patte() {
    delete griffe;
}

void Patte::init() {

	float palm_l = 0.5;
	float palm_w = 0.3;
	float palm_h = 0.2;
	float finger_l = 1.2;
	float finger_w = 0.1;
	float finger_h = 0.198;

	ver_p[0][0] = -palm_l/2;
	ver_p[0][1] = -palm_w;
	ver_p[0][2] = -palm_h;
	ver_p[1][0] = -palm_l/2;
	ver_p[1][1] = -palm_w;
	ver_p[1][2] = palm_h;
	ver_p[2][0] = -palm_l;
	ver_p[2][1] = -palm_w/2;
	ver_p[2][2] = -palm_h;
	ver_p[3][0] = -palm_l;
	ver_p[3][1] = -palm_w/2;
	ver_p[3][2] = palm_h;
	ver_p[4][0] = -palm_l;
	ver_p[4][1] = palm_w/2;
	ver_p[4][2] = -palm_h;
	ver_p[5][0] = -palm_l;
	ver_p[5][1] = palm_w/2;
	ver_p[5][2] = palm_h;
	ver_p[6][0] = -palm_l/2;
	ver_p[6][1] = palm_w;
	ver_p[6][2] = -palm_h;
	ver_p[7][0] = -palm_l/2;
	ver_p[7][1] = palm_w;
	ver_p[7][2] = palm_h;
	ver_p[8][0] = palm_l;
	ver_p[8][1] = palm_w;
	ver_p[8][2] = -palm_h;
	ver_p[9][0] = palm_l;
	ver_p[9][1] = palm_w;
	ver_p[9][2] = palm_h;
	ver_p[10][0] = palm_l;
	ver_p[10][1] = -palm_w;
	ver_p[10][2] = -palm_h;
	ver_p[11][0] = palm_l;
	ver_p[11][1] = -palm_w;
	ver_p[11][2] = palm_h;

	ver_p[12][0] = 0;
	ver_p[12][1] = -finger_w;
	ver_p[12][2] = -finger_h;
	ver_p[13][0] = 0;
	ver_p[13][1] = -finger_w;
	ver_p[13][2] = finger_h;
	ver_p[14][0] = 0;
	ver_p[14][1] = finger_w;
	ver_p[14][2] = -finger_h;
	ver_p[15][0] = 0;
	ver_p[15][1] = finger_w;
	ver_p[15][2] = finger_h;
	ver_p[16][0] = finger_l;
	ver_p[16][1] = finger_w;
	ver_p[16][2] = -finger_h;
	ver_p[17][0] = finger_l;
	ver_p[17][1] = finger_w;
	ver_p[17][2] = finger_h;
	ver_p[18][0] = finger_l;
	ver_p[18][1] = -finger_w;
	ver_p[18][2] = -finger_h;
	ver_p[19][0] = finger_l;
	ver_p[19][1] = -finger_w;
	ver_p[19][2] = finger_h;

	nor_p[0][0] = -1;
	nor_p[0][1] = -1;
	nor_p[0][2] = -1;
	nor_p[1][0] = -1;
	nor_p[1][1] = -1;
	nor_p[1][2] = 1;
	nor_p[2][0] = -1;
	nor_p[2][1] = 1;
	nor_p[2][2] = -1;
	nor_p[3][0] = -1;
	nor_p[3][1] = 1;
	nor_p[3][2] = 1;
	nor_p[4][0] = 1;
	nor_p[4][1] = 1;
	nor_p[4][2] = -1;
	nor_p[5][0] = 1;
	nor_p[5][1] = 1;
	nor_p[5][2] = 1;
	nor_p[6][0] = 1;
	nor_p[6][1] = -1;
	nor_p[6][2] = -1;
	nor_p[7][0] = 1;
	nor_p[7][1] = -1;
	nor_p[7][2] = 1;

	nor_p[8][0] = 0;
	nor_p[8][1] = -1;
	nor_p[8][2] = -1;
	nor_p[9][0] = 0;
	nor_p[9][1] = -1;
	nor_p[9][2] = 1;
	nor_p[10][0] = -1;
	nor_p[10][1] = 0;
	nor_p[10][2] = -1;
	nor_p[11][0] = -1;
	nor_p[11][1] = 0;
	nor_p[11][2] = 1;
	nor_p[12][0] = 0;
	nor_p[12][1] = 1;
	nor_p[12][2] = -1;
	nor_p[13][0] = 0;
	nor_p[13][1] = 1;
	nor_p[13][2] = 1;

}

/* Dessin d'un "doigt de pied" */
void Patte::drawRectangle(int i) {

    /* Coordonnées pour la texture */
    GLfloat tex_x = 0;
    GLint tex_y = 0;

	glBegin(GL_QUAD_STRIP);
   
        glTexCoord2d(tex_x, tex_y); tex_y = tex_y ^ 1; 
        glNormal3fv(nor_p[8]);
        glVertex3fv(ver_p[0 + i]);
        
        glTexCoord2d(tex_x, tex_y); tex_x += 0.5; tex_y = tex_y ^ 1; 
        glNormal3fv(nor_p[9]);
        glVertex3fv(ver_p[1 + i]);
        
        glTexCoord2d(tex_x, tex_y); tex_y = tex_y ^ 1; 
        glNormal3fv(nor_p[12]);
        glVertex3fv(ver_p[2 + i]);
        
        glTexCoord2d(tex_x, tex_y); tex_x += 1.5; tex_y = tex_y ^ 1; 
        glNormal3fv(nor_p[13]);
        glVertex3fv(ver_p[3 + i]);
        
        glTexCoord2d(tex_x, tex_y); tex_y = tex_y ^ 1; 
        glNormal3fv(nor_p[4]);
        glVertex3fv(ver_p[4 + i]);
        
        glTexCoord2d(tex_x, tex_y); tex_x += 0.5; tex_y = tex_y ^ 1; 
        glNormal3fv(nor_p[5]);
        glVertex3fv(ver_p[5 + i]);
        
        glTexCoord2d(tex_x, tex_y); tex_y = tex_y ^ 1; 
        glNormal3fv(nor_p[6]);
        glVertex3fv(ver_p[6 + i]);
        
        glTexCoord2d(tex_x, tex_y); tex_x += 1.5; tex_y = tex_y ^ 1; 
        glNormal3fv(nor_p[7]);
        glVertex3fv(ver_p[7 + i]);
        
        glTexCoord2d(tex_x, tex_y); tex_y = tex_y ^ 1; 
        glNormal3fv(nor_p[8]);
        glVertex3fv(ver_p[0 + i]);
        
        glTexCoord2d(tex_x, tex_y); 
        glNormal3fv(nor_p[9]);
        glVertex3fv(ver_p[1 + i]);

	glEnd();

	glBegin(GL_POLYGON);

    	glTexCoord2d(0, 0);
        glNormal3fv(nor_p[9]);
        glVertex3fv(ver_p[1 + i]);
    	
        glTexCoord2d(0, 1);
        glNormal3fv(nor_p[13]);
        glVertex3fv(ver_p[3 + i]);
    	
        glTexCoord2d(2, 1);
        glNormal3fv(nor_p[5]);
        glVertex3fv(ver_p[5 + i]);
    	
        glTexCoord2d(2, 0);
        glNormal3fv(nor_p[7]);
        glVertex3fv(ver_p[7 + i]);

	glEnd();

	glBegin(GL_POLYGON);

    	glTexCoord2d(0, 0);
    	glNormal3fv(nor_p[8]);
        glVertex3fv(ver_p[0 + i]);

    	glTexCoord2d(0, 1);
        glNormal3fv(nor_p[12]);
        glVertex3fv(ver_p[2 + i]);

    	glTexCoord2d(2, 1);
        glNormal3fv(nor_p[4]);
        glVertex3fv(ver_p[4 + i]);

    	glTexCoord2d(2, 0);
        glNormal3fv(nor_p[6]);
        glVertex3fv(ver_p[6 + i]);

	glEnd();
}

void Patte::draw() {

    glEnable(GL_TEXTURE_2D);

    /* On bind la texture */
    glBindTexture(GL_TEXTURE_2D, tex_peau);
    /* Répétition sur les x */
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    /* Pas de répétition sur les y */
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

    /* Couleur utilisée pour la peau */
    glColor3f(color_R, color_G, color_B);

	glPushMatrix();

	glScalef(taille, taille, taille);

    /***************************************************************/
    /* Dessin de l'arrière du pied */
	glBegin(GL_QUAD_STRIP);
    
        /* Coordonnées d'application de la texture */
        GLfloat tex_x = 0;
        GLint tex_y = 0;

        GLint k = 8;
        for(GLint i = 0; i < 12; i++) {
            /* Coordonnées de la texture */
            glTexCoord2d(tex_x, tex_y);
            /* Normale */
            glNormal3fv(nor_p[k]);
            /* Vecteur */
            glVertex3fv(ver_p[i]);
            
            if(i == 3 || i == 7) {
                k--;
            } else if(i == 9) {
                k = 8;
            }else {
                k++;
            }

            tex_x += 0.5;
            tex_y = tex_y ^ 1;
        }
        /* Fermeture */
        glTexCoord2d(tex_x, tex_y);
        glNormal3fv(nor_p[8]);
        glVertex3fv(ver_p[0]);
        tex_x += 0.5;
        tex_y = tex_y ^ 1;
        
        glTexCoord2d(tex_x, tex_y);
        glNormal3fv(nor_p[9]);
        glVertex3fv(ver_p[1]);

	glEnd();

    /***************************************************************/
    /* Dessin du dessus du pied */
	glBegin(GL_POLYGON);
        
        /* On définit le centre (de référence) de la texture */
        GLfloat tex_centre_x = 0.5;
        GLfloat tex_centre_y = 0.5;

        /* Coordonnées Texture */
        glTexCoord2d(tex_centre_x + ver_p[1][0], tex_centre_y + ver_p[1][1]);
        glNormal3fv(nor_p[9]);
        glVertex3fv(ver_p[1]);

        glTexCoord2d(tex_centre_x + ver_p[3][0], tex_centre_y + ver_p[3][1]);
        glNormal3fv(nor_p[11]);
        glVertex3fv(ver_p[3]);

        glTexCoord2d(tex_centre_x + ver_p[5][0], tex_centre_y + ver_p[5][1]);
        glNormal3fv(nor_p[11]);
        glVertex3fv(ver_p[5]);

        glTexCoord2d(tex_centre_x + ver_p[7][0], tex_centre_y + ver_p[7][1]);
        glNormal3fv(nor_p[13]);
        glVertex3fv(ver_p[7]);

        glTexCoord2d(tex_centre_x + ver_p[9][0], tex_centre_y + ver_p[9][1]);
        glNormal3fv(nor_p[13]);
        glVertex3fv(ver_p[9]);

        glTexCoord2d(tex_centre_x + ver_p[11][0], tex_centre_y + ver_p[11][1]);
        glNormal3fv(nor_p[9]);
        glVertex3fv(ver_p[11]);

	glEnd();

    /***************************************************************/
    /* Dessous de la patte */
	glBegin(GL_POLYGON);

        glTexCoord2d(tex_centre_x + ver_p[0][0], tex_centre_y + ver_p[0][1]);
        glNormal3fv(nor_p[8]);
        glVertex3fv(ver_p[0]);

        glTexCoord2d(tex_centre_x + ver_p[2][0], tex_centre_y + ver_p[2][1]);
        glNormal3fv(nor_p[10]);
        glVertex3fv(ver_p[2]);

        glTexCoord2d(tex_centre_x + ver_p[4][0], tex_centre_y + ver_p[4][1]);
        glNormal3fv(nor_p[10]);
        glVertex3fv(ver_p[4]);

        glTexCoord2d(tex_centre_x + ver_p[6][0], tex_centre_y + ver_p[6][1]);
        glNormal3fv(nor_p[12]);
        glVertex3fv(ver_p[6]);

        glTexCoord2d(tex_centre_x + ver_p[8][0], tex_centre_y + ver_p[8][1]);
        glNormal3fv(nor_p[12]);
        glVertex3fv(ver_p[8]);

        glTexCoord2d(tex_centre_x + ver_p[10][0], tex_centre_y + ver_p[10][1]);
        glNormal3fv(nor_p[8]);
        glVertex3fv(ver_p[10]);

	glEnd();

    /***************************************************************/
    /* Dessin des "doigts de pieds" et des griffes */
	glPushMatrix();
	drawRectangle(12);
	glTranslatef(1.2, 0, 0);
	glRotatef(-90, 0, 0, 1);
	glRotatef(-90, 0, 1, 0);
    /* On "désactive" la couleur */
    glColor3f(1, 1, 1);
	griffe->draw();
    /* On "réactive" la couleur et la texture */
    glColor3f(color_R, color_G, color_B);
    glEnable(GL_TEXTURE_2D);
	glPopMatrix();

    /* On bind la texture */
    glBindTexture(GL_TEXTURE_2D, tex_peau);
    /* Répétition sur les x */
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    /* Pas de répétition sur les y */
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

	glPushMatrix();
	glRotatef(-25, 0, 0, 1);
	drawRectangle(12);
	glTranslatef(1.2, 0, 0);
	glRotatef(-90, 0, 0, 1);
	glRotatef(-90, 0, 1, 0);
    /* On "désactive" la couleur */
    glColor3f(1, 1, 1);
	griffe->draw();
    /* On "réactive" la couleur et la texture */
    glColor3f(color_R, color_G, color_B);
    glEnable(GL_TEXTURE_2D);
	glPopMatrix();

    /* On bind la texture */
    glBindTexture(GL_TEXTURE_2D, tex_peau);
    /* Répétition sur les x */
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    /* Pas de répétition sur les y */
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

	glPushMatrix();
	glRotatef(25, 0, 0, 1);
	drawRectangle(12);
	glTranslatef(1.2, 0, 0);
	glRotatef(-90, 0, 0, 1);
    /* On "désactive" la couleur */
    glColor3f(1, 1, 1);
	glRotatef(-90, 0, 1, 0);
	griffe->draw();
	glPopMatrix();

	glPopMatrix();

    glColor3f(1, 1, 1);
    glDisable(GL_TEXTURE_2D);
}
