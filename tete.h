#ifndef _TETE_
#define _TETE_

#include "renderable.h"
#include "corne.h"
#ifndef __APPLE__
#include <GL/glut.h>
#else
#include <GLUT/glut.h>
#endif

class Tete : public Renderable {
public:
    void init();
    void draw();
    Tete(float taille, GLuint texture_cornes, GLfloat r, GLfloat g, GLfloat b,
            GLuint texture_peau);
    ~Tete();

private:
    float taille;
    GLfloat ver_t[8][3];
    GLfloat nor_t[8][3];
    Corne *c1;
    Corne *c2;
    Corne *c3;

    /* Couleur */
    GLfloat color_R;
    GLfloat color_G;
    GLfloat color_B;

    /* Texture */
    GLuint tex_peau;
};

#endif

