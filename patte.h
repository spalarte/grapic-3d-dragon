#ifndef _PATTE_
#define _PATTE_

#include "renderable.h"
#ifndef __APPLE__
#include <GL/glut.h>
#else
#include <GLUT/glut.h>
#endif
#include "griffe.h"

class Patte : public Renderable {
public:
    void init();
    void drawRectangle(int i);
    void draw();
    Patte(float taille, GLuint texture,
            GLfloat r, GLfloat g, GLfloat b, GLuint tex_griffe);
    ~Patte();

private:
    float taille;
    GLfloat ver_p[20][3];
    GLfloat nor_p[14][3];
    Griffe *griffe;

    /* Texture */
    GLint tex_peau;

    /* Couleur de la peau */
    GLfloat color_R;
    GLfloat color_G;
    GLfloat color_B;

};

#endif

