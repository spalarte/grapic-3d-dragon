#ifndef _GRIFFE_
#define _GRIFFE_

#include "renderable.h"
#ifndef __APPLE__
#include <GL/glut.h>
#else
#include <GLUT/glut.h>
#endif

class Griffe : public Renderable {
public:
    void init();
    void draw();
    Griffe(float rayon1, float rayon2, float angle, GLuint texture);

private:
    float rayon1;
    float rayon2;
    float angle;
    GLfloat ver_g[6][11][3];
    GLfloat nor_g[6][11][3];

    /* Texture */
    GLuint tex_griffe;
    GLfloat textures[6][11][2];
    void init_textures();
};

#endif

