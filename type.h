#ifndef _TYPE_
#define _TYPE_

#include "renderable.h"
#ifndef __APPLE__
#include <GL/glut.h>
#else
#include <GLUT/glut.h>
#endif

class Type : public Renderable {
public:
    void draw();

private:

};

#endif

