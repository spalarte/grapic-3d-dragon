#ifndef _NECK_
#define _NECK_

#include "renderable.h"
#ifndef __APPLE__
#include <GL/glut.h>
#else
#include <GLUT/glut.h>
#endif

class Neck : public Renderable {
public:
    void init();
    void draw();
    Neck(float base1, float hauteur1, float base2,
            float hauteur2, float base3, GLuint texture,
            GLfloat r, GLfloat g, GLfloat b);

private:
    float base1;
    float hauteur1;
    float base2;
    float hauteur2;
    float base3;
    
    GLfloat ver_n[12][3];
    GLfloat nor_n[12][3];

    /* Texture */
    GLuint tex_peau;
    /* Couleur */
    GLfloat color_R;
    GLfloat color_G;
    GLfloat color_B;
};

#endif

