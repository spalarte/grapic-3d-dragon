#ifndef _CYLINDER_
#define _CYLINDER_

#include "renderable.h"
#ifndef __APPLE__
#include <GL/glut.h>
#else
#include <GLUT/glut.h>
#endif

class Cylinder : public Renderable {
public:
    void draw();
    Cylinder(int x, int y);
private:
    void drawImmediate();
    void drawElements();
    void drawArrays();

    int deb, fin;

};

#endif

