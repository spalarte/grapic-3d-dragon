#include <iostream>
#include <numeric>
using namespace std;
#include "math.h"
#include "leg.h"

Leg::Leg(float taille, GLuint texture, GLfloat r, GLfloat g, GLfloat b) {
	this->taille = taille;

    /* Chargement de la texture */
    tex_peau = texture;

    /* Couleur */
    color_R = r;
    color_G = g;
    color_B = b;
}

void Leg::draw() {
    
    glEnable(GL_TEXTURE_2D);
    glColor3f(color_R, color_G, color_B);

    /* On bind la texture */
    glBindTexture(GL_TEXTURE_2D, tex_peau);

    /* Texture va être plaquée sur une sphère */
    GLUquadric *params = gluNewQuadric();
    gluQuadricTexture(params, GL_TRUE);     // on applique une texture
    gluQuadricDrawStyle(params, GL_FILL);   // objet plein

	glPushMatrix();

	glScalef(taille, taille, taille);

	glTranslatef(2, 0, 0);
	glPushMatrix();
	glScalef(2.3, 1, 1);
	gluSphere(params, 1, 20, 20);
	glPopMatrix();

	glTranslatef(0.8, 0, -1.3);
	glRotatef(-50, 0, 1, 0);
	glScalef(3.3, 1, 1);
	gluSphere(params, 0.6, 20, 20);

    /* Suppresion paramétrage texture */
    gluDeleteQuadric(params);

	glPopMatrix();

    glColor3f(1, 1, 1);
    glDisable(GL_TEXTURE_2D);
}
