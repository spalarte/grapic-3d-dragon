#include <iostream>
#include <cmath>
using namespace std;
#include "tail.h"

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

/* Initialisation des points et des normales d'un cercle */
void Tail::init_cercle(GLfloat r, GLint index, GLfloat z, GLfloat dx, GLfloat tex_x)
{
    /* Angle courant */
    GLfloat current_angle = 0;

    /* Indice du cercle pour la texture */
    GLint tex_i = (index / (NB_VERTEX * 3)) * (NB_VERTEX * 2);
    /* Décalage suivant y (texture) */
    GLfloat tex_dy = 1.0 / (NB_VERTEX / 2 + 1);
    /* Texture : valeur de y */
    GLfloat tex_y = 1;
    bool decr_tex_y = true;

    for(GLint i = index; i < index + NB_VERTEX * 3; i += 3) {

        /* Coordonnées du point */
        vertices_elem[i] = cos(current_angle) * r + dx;
        vertices_elem[i + 1] = sin(current_angle) * r;
        vertices_elem[i + 2] = z;

        /* Coordonnées des vecteurs normaux */
        normals_elem[i] = cos(current_angle);
        normals_elem[i + 1] = sin(current_angle);
        normals_elem[i + 2] = 0;

        /* Coordonnées de la texture à appliquer à ce point */
        textures_elem[tex_i] = tex_x;
        textures_elem[tex_i + 1] = tex_y;
        tex_i += 2;
        if(decr_tex_y) {
            tex_y -= tex_dy;
            if(tex_y < 0) {
                tex_y = 0;
                decr_tex_y = false;
            }
        } else {
            tex_y += tex_dy;
        }

        current_angle += angle;
    }
}

/* Initialisation des indices utilisés */
void Tail::init_indices(GLint cercle1, GLint cercle2, index_t *id)
{
    /* Utilisation d'un quad_strip :
     * 0--2-- .. --0
     * |  |        |
     * 1--3-- .. --1
     */

    for(GLint i = 0; i < NB_VERTEX * 2; i += 2) {
        id->elem[i] = cercle1 * NB_VERTEX + (i / 2);
        id->elem[i + 1] = cercle2 * NB_VERTEX + (i / 2);
    }
    id->elem[NB_VERTEX * 2] = cercle1 * NB_VERTEX;
    id->elem[NB_VERTEX * 2 + 1] = cercle2 * NB_VERTEX;
}

/* Initialisation des coordonnées de texture de la pointe de la queue */
void Tail::init_cone()
{
    /* Cone = un triangle rectangle qui tourne sur son axe
     *  |\
     * 1| \d
     *  |__\
     *   r
     */

    /* d² = r² + 1² */
    GLfloat d = sqrt(cone_r * cone_r + 1);

    /* longueur arc proportionnel angle au centre */
    GLfloat p = 2 * M_PI * cone_r;
    GLfloat angle_centre_total = ((360 * p) / (2 * M_PI * d));  // degrés
    GLfloat angle_tex = (angle_centre_total/NB_VERTEX) * (M_PI / 180);

    GLfloat angle_cour = 0;

    for(GLint i = 0; i < NB_VERTEX * 2; i += 2) {
        textures_cone[i] = cos(angle_cour) * d;
        textures_cone[i + 1] = sin(angle_cour) * d;

        angle_cour += angle_tex;
    }
}

/* Constructeur */
Tail::Tail(float taille, GLuint texture, GLfloat r, GLfloat g, GLfloat b)
{
    this->taille = taille;
    this->angle2 = -18;
    this->sens = -1;

    /* Angle entre deux points d'un cercle */
    angle = (360/NB_VERTEX) * (M_PI/180);

    /* Rayon courant du cercle */
    GLfloat rayon = 1;

    /* Premier indice */
    GLint indice = 0;

    /* Hauteur courante */
    GLfloat z = 0;

    /* Décalage courant par rapport à l'axe des x */
    GLfloat dx = 0;

    /* Création des cercles */
    GLfloat tex_x = 0;
    for(GLint i = 0; i < NB_CERCLES - 1; i++) {
        init_cercle(rayon, indice, z, dx, tex_x);

        /* Mise à jour des variables */
        z += 1;
        indice += NB_VERTEX * 3;
        rayon -= 0.1;
        if(i < NB_CERCLES / 2 - 2) {
            dx += 0.1;
        } else if(i < NB_CERCLES / 2) {
            dx += 0.05;
        } else if(i < NB_CERCLES / 2 + 2) {
            dx += 0.05;
        } else {
            dx -= 0.1;
        }
        tex_x += 1;
    }
    init_cercle(rayon, indice, z, dx, tex_x);

    /* Position du centre du dernier cercle */
    cone_x = dx;
    cone_z = z;
    cone_r = rayon;

    /* Définition des indices pour le dessin */
    index_t *id_cour = new index_t;
    indices = id_cour;

    for(GLint i = 1; i < NB_CERCLES - 1; i++) {
        init_indices(i - 1, i, id_cour);
        id_cour->next = new index_t;
        id_cour = id_cour->next;
    }
    init_indices(NB_CERCLES - 2, NB_CERCLES - 1, id_cour);

    /* Initialisation des coordonnées de texture pour la pointe */
    init_cone();

    /* Chargement de la texture */
    tex_peau = texture;

    /* Couleur */
    color_R = r;
    color_G = g;
    color_B = b;
}

/* Destructeur */
Tail::~Tail()
{
    index_t *id_cour = indices;
    while(id_cour != NULL) {
        index_t *prec = id_cour;
        id_cour = id_cour->next;
        delete(prec);
    }
}

/* Affichage */
void Tail::draw()
{
    glEnable(GL_TEXTURE_2D);
    glColor3f(color_R, color_G, color_B);

    // On utilise la texture de peau de serpent
    glBindTexture(GL_TEXTURE_2D, tex_peau);
    // Répétition sur les x
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    // Pas de répétition sur les y
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

    glPushMatrix();

    glScalef(taille, taille, taille);
    glRotatef(this->angle2, 0, 1, 0);

    /* Dessin de la queue */
    
    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(3, GL_FLOAT, 0, vertices_elem);

    glEnableClientState(GL_NORMAL_ARRAY);
    glNormalPointer(GL_FLOAT, 0, normals_elem);

    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    glTexCoordPointer(2, GL_FLOAT, 0, textures_elem);

    index_t *id_cour = indices;
    while(id_cour != NULL) {
        glDrawElements(GL_QUAD_STRIP, (NB_VERTEX + 1) * 2, GL_UNSIGNED_BYTE, id_cour->elem);
        id_cour = id_cour->next;
    }

    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    glDisableClientState(GL_NORMAL_ARRAY);
    glDisableClientState(GL_VERTEX_ARRAY);

    /* Cone terminant la queue */
    draw_pointe();

    glPopMatrix();

    glColor3f(1, 1, 1);
    glDisable(GL_TEXTURE_2D);
}

void Tail::draw_pointe()
{
    GLint decalage = NB_VERTEX * 3 * (NB_CERCLES - 1);  // décalage si 3 coordonnées

    glBegin(GL_TRIANGLE_FAN);

        // Centre
        glTexCoord2d(0, 0);
        glNormal3f(0, 0, 0);
        glVertex3f(0, 0, cone_z + 1);

        // Tour
        GLint k = 0;
        for(GLint i = 0; i < NB_VERTEX * 3; i += 3) {
            glTexCoord2d(textures_cone[k], textures_cone[k + 1]);
            k += 2;
            glNormal3f(normals_elem[decalage + i], normals_elem[decalage + i + 1], 0);
            glVertex3f(vertices_elem[decalage + i], vertices_elem[decalage + i + 1], cone_z);
        }

        // Fin du cône
        glTexCoord2d(textures_cone[0], textures_cone[1]);
        glNormal3f(normals_elem[decalage], normals_elem[decalage + 1], 0);
        glVertex3f(vertices_elem[decalage], vertices_elem[decalage + 1], cone_z);

    glEnd();
}

void Tail::animateTail() 
{
	if (this->angle2 >= 18 or this->angle2 <= -18) {
		this->sens = -this->sens;
		this->angle2 += this->sens;
	} else {
		this->angle2 += this->sens;
	}
}


