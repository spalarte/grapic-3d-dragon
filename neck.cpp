#include <iostream>
#include <numeric>
using namespace std;
#include "neck.h"

Neck::Neck(float base1, float hauteur1, float base2, float hauteur2, float base3,
        GLuint texture, GLfloat r, GLfloat g, GLfloat b) {
	this->base1 = base1;
	this->hauteur1 = hauteur1;
	this->base2 = base2;
	this->hauteur2 = hauteur2;
	this->base3 = base3;
	init();

    /* Texture */
    tex_peau =  texture;

    /* Couleur */
    color_R = r;
    color_G = g;
    color_B = b;
}

void Neck::init() {

	ver_n[0][0] = -base1;
	ver_n[0][1] = -base1;
	ver_n[0][2] = 0;
	ver_n[1][0] = -base2;
	ver_n[1][1] = -base2;
	ver_n[1][2] = hauteur1;
	ver_n[2][0] = -base1;
	ver_n[2][1] = base1;
	ver_n[2][2] = 0;
	ver_n[3][0] = -base2;
	ver_n[3][1] = base2;
	ver_n[3][2] = hauteur1;
	ver_n[4][0] = base1;
	ver_n[4][1] = base1;
	ver_n[4][2] = 0;
	ver_n[5][0] = base2;
	ver_n[5][1] = base2;
	ver_n[5][2] = hauteur1;
	ver_n[6][0] = base1;
	ver_n[6][1] = -base1;
	ver_n[6][2] = 0;
	ver_n[7][0] = base2;
	ver_n[7][1] = -base2;
	ver_n[7][2] = hauteur1;
	ver_n[8][0] = -base3;
	ver_n[8][1] = -base3;
	ver_n[8][2] = hauteur2;
	ver_n[9][0] = -base3;
	ver_n[9][1] = base3;
	ver_n[9][2] = hauteur2;
	ver_n[10][0] = base3;
	ver_n[10][1] = base3;
	ver_n[10][2] = hauteur2;
	ver_n[11][0] = base3;
	ver_n[11][1] = -base3;
	ver_n[11][2] = hauteur2;

	nor_n[0][0] = -1;
	nor_n[0][1] = -1;
	nor_n[0][2] = 1;
	nor_n[1][0] = -2;
	nor_n[1][1] = -2;
	nor_n[1][2] = 1;
	nor_n[2][0] = -1;
	nor_n[2][1] = 1;
	nor_n[2][2] = 1;
	nor_n[3][0] = -2;
	nor_n[3][1] = 2;
	nor_n[3][2] = 1;
	nor_n[4][0] = 1;
	nor_n[4][1] = 1;
	nor_n[4][2] = 1;
	nor_n[5][0] = 2;
	nor_n[5][1] = 2;
	nor_n[5][2] = 1;
	nor_n[6][0] = 1;
	nor_n[6][1] = -1;
	nor_n[6][2] = 1;
	nor_n[7][0] = 2;
	nor_n[7][1] = -2;
	nor_n[7][2] = 1;
	nor_n[8][0] = -1;
	nor_n[8][1] = -1;
	nor_n[8][2] = 0;
	nor_n[9][0] = -1;
	nor_n[9][1] = 1;
	nor_n[9][2] = 0;
	nor_n[10][0] = 1;
	nor_n[10][1] = 1;
	nor_n[10][2] = 0;
	nor_n[11][0] = 1;
	nor_n[11][1] = -1;
	nor_n[11][2] = 0;

}

void Neck::draw() {

    glEnable(GL_TEXTURE_2D);
    glColor3f(color_R, color_G, color_B);

    /* On bind la texture utilisée */
    glBindTexture(GL_TEXTURE_2D, tex_peau);
    /* Répétition sur les x */
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    /* Pas de répétition sur les y */
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

	glPushMatrix();

    /* Coordonnées courantes de la texture */
    GLfloat tex_x = 1;
    GLint tex_y = 0;

    /* Base du cou */
	glBegin(GL_QUAD_STRIP);

	    for (int i = 0; i<8; i++) {
            glTexCoord2d(tex_x, tex_y);
            glNormal3fv(nor_n[i]);
            glVertex3fv(ver_n[i]);

            if(i % 2 == 1) {
                tex_x -= 0.5;
            }
            tex_y = tex_y ^ 1;
        }
        glTexCoord2d(tex_x, tex_y);
        glNormal3fv(nor_n[0]);
        glVertex3fv(ver_n[0]);	
        
        tex_y = tex_y ^ 1;

        glTexCoord2d(tex_x, tex_y);
        glNormal3fv(nor_n[1]);
        glVertex3fv(ver_n[1]);
    
    glEnd();

    /* RAZ des coordonnées courantes de la texture */
    tex_x = 1;
    tex_y = 0;

    /* Longueur du cou */
	glBegin(GL_QUAD_STRIP);

        for (int i = 0; i<4; i++) {
            glTexCoord2d(tex_x, tex_y);
            glNormal3fv(nor_n[2*i + 1]);
            glVertex3fv(ver_n[2*i + 1]);
    
            tex_y = tex_y ^ 1;

            glTexCoord2d(tex_x, tex_y);
            glNormal3fv(nor_n[i+8]);
            glVertex3fv(ver_n[i+8]);

            tex_x -= 0.5;
            tex_y = tex_y ^ 1;
        }
        glTexCoord2d(tex_x, tex_y);
        glNormal3fv(nor_n[1]);
        glVertex3fv(ver_n[1]);
    
        tex_y = tex_y ^ 1;

        glTexCoord2d(tex_x, tex_y);
        glNormal3fv(nor_n[8]);
        glVertex3fv(ver_n[8]);

	glEnd();

	glPopMatrix();

    glColor3f(1, 1, 1);
    glDisable(GL_TEXTURE_2D);
}
