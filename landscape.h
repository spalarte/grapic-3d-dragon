#ifndef _LAND_
#define _LAND_

#include <QGLViewer/vec.h>
#include "renderable.h"
#include "cube.h"
#include "wing.h"
#include "griffe.h"
#include "corne.h"
#include "patte.h"
#include "tete.h"
#include "tail.h"
#include "body.h"
#include "neck.h"
#include "leg.h"
#include "feu.h"
#include "leash.h"
#ifndef __APPLE__
#include <GL/glut.h>
#else
#include <GLUT/glut.h>
#endif

class Landscape : public Renderable {
public:
    void draw();
    void animate();
    void init(Viewer &viewer);
    void keyPressEvent(QKeyEvent*, Viewer&);
private:
    // System parameters (common)
    qglviewer::Vec defaultGravity;
    qglviewer::Vec gravity; // gravity used in simulation
    double defaultMediumViscosity;
    double mediumViscosity; // viscosity used in simulation
    double dt; // time step
    bool handleCollisions;

    // Collisions parameters
    qglviewer::Vec groundPosition;
    qglviewer::Vec groundNormal;
    double rebound; // 0 = pure absorption; 1 = pure elastic impact

    //body
    Wing * wing;
    Patte * patte;
    Tete * tete;
    Tail * tail;
    Body * body;
    Neck * neck;
    Leg * leg;
    Feu * feu;
    Leash * leash;

    // Textures
    GLuint tex_peau;
    GLuint tex_corne;
    GLuint tex_landscape;
    GLuint tex_aile;

    //draws
    void drawLandscape();
    void drawDragon();

    // Animation
    int cycle;
    float angle;
    float rayon;
    float hauteur;
    float sens;
};

#endif

