#ifndef _TEXTURES_UTIL_H_
#define _TEXTURES_UTIL_H_

#ifndef __APPLE__
#include <GL/glut.h>
#else
#include <GLUT/glut.h>
#endif

#include <QImage>

GLuint loadTexture(const char *filename, bool mipmap=false);
GLuint loadCubeMapTexture(const char *top, const char *bottom,
        const char *left, const char *front, const char *right,
        const char *back);

#endif
