#ifndef _PARTICULE_
#define _PARTICULE_

#include "renderable.h"
#include <cstdlib>
#ifndef __APPLE__
#include <GL/glut.h>
#else
#include <GLUT/glut.h>
#endif

class Particule : public Renderable {
public:
	float myRandom(float min, float max);
	void reinit();
	void draw();
	void animateParticule();
	Particule();

private:
	bool active; // Active (1=Oui/0=Non)
	double life; // Durée de vie
    int life_cycle;
	double fade; // Vitesse de disparition
	double r, g, b; // Valeurs RGB pour la couleur
	double x, y, z; // Position
	double xi, yi, zi; // Vecteur de déplacement
	double xg, yg, zg; // Gravitébool active; // Active (1=Oui/0=Non)

};

#endif

