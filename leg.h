#ifndef _LEG_
#define _LEG_

#include "renderable.h"
#ifndef __APPLE__
#include <GL/glut.h>
#else
#include <GLUT/glut.h>
#endif

class Leg : public Renderable {
public:
    void draw();
    Leg(float taille, GLuint texture, GLfloat r, GLfloat g, GLfloat b);

private:
    float taille;
    GLuint tex_peau;
    GLfloat color_R;
    GLfloat color_G;
    GLfloat color_B;

};

#endif

