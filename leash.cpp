#include <iostream>
using namespace std;
#include "leash.h"
#define NB_PARTICLE 8

Leash::Leash(qglviewer::Vec gravity, double mediumViscosity, double dt) {
    // parameters shared by all particles
    particleMass = 4;
    particleRadius = 1;
    distanceBetweenParticles = 1.5 * particleRadius;
    // parameters shared by all springs
    springStiffness = 220.0;
    springInitLength = 2 * particleRadius;
    springDamping = 0.1;
    this->gravity = gravity;
    this->mediumViscosity = mediumViscosity;
    this->dt = dt;
    rebound = 1;

    createSystemScene();
}

void Leash::draw() {
    glPushMatrix();
    // Springs
    glColor3f(0.3, 0.3, 0.3);
    glLineWidth(5.0);
    std::vector<Spring *>::iterator itS;
    glScalef(0.5, 0.5, 0.5);
    for (itS = springs.begin(); itS != springs.end(); ++itS) {

        (*itS)->draw();
    }

    glPopMatrix();
}

void Leash::createSystemScene() {
    // add a fixed particle
    qglviewer::Vec initPos = qglviewer::Vec(0.0, 0.0, 0);
    particles.push_back(new Particle(initPos, qglviewer::Vec(), 0, particleRadius));
    qglviewer::Vec pos1 = initPos - qglviewer::Vec(0, 0, distanceBetweenParticles);
    qglviewer::Vec vel1 = qglviewer::Vec(0, 2, 0); // non null initial velocity
    for (int i = 0; i < NB_PARTICLE - 1; i++) {
        particles.push_back(new Particle(pos1, vel1, particleMass, particleRadius));
        springs.push_back(new Spring(particles.at(i), particles.at(i + 1),
                springStiffness, springInitLength, springDamping, particleRadius));
        pos1 = pos1 - qglviewer::Vec(0, 0, distanceBetweenParticles);
    }
}

void Leash::animateLeash() {
    //======== 1. Compute all forces
    // map to accumulate the forces to apply on each particle
    std::map<const Particle *, qglviewer::Vec> forces;

    // weights
    std::vector<Particle *>::iterator itP;
    for (itP = particles.begin(); itP != particles.end(); ++itP) {
        Particle *p = *itP;
        forces[p] = gravity * p->getMass();
    }

    // damped springs
    std::vector<Spring *>::iterator itS;
    for (itS = springs.begin(); itS != springs.end(); ++itS) {
        Spring *s = *itS;
        qglviewer::Vec f12 = s->getCurrentForce();

        forces[s->getParticle1()] += f12;
        forces[s->getParticle2()] -= f12; // opposite force

    }

    //    viscosity
    for (itP = particles.begin(); itP != particles.end(); ++itP) {
        Particle *p = *itP;
        forces[p] += (-mediumViscosity) * p->getVelocity();
    }


    //======== 2. Integration scheme
    // update particles velocity (qu. 1)
    for (itP = particles.begin(); itP != particles.end(); ++itP) {
        Particle *p = *itP;
        qglviewer::Vec a = p->getInvMass() * forces[p];
        p->incrVelocity(dt * a);
    }

    // update particles positions
    for (itP = particles.begin(); itP != particles.end(); ++itP) {
        Particle *p = *itP;
        //q = q + dt * v;
        p->incrPosition(dt * p->getVelocity());
    }

}


