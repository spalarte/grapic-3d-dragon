#include <iostream>
#include <cmath>
using namespace std;
#include "body.h"

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

#define PRECISION 100

#define ANGLE_MIN 75
#define ANGLE_MAX 105
#define R_LARGE 10
#define DY 9

#define Z_UP 1
#define Z_DOWN 0

#define PREC 32
#define DOS_DZ_I 0.07
#define DZ_DOS 0.03

#define VENTRE_DZ_I 0.08
#define DZ_VENTRE 0.1

#define AV_DX_I 0.2
#define DX_AV 0.07

/* Initialisation des flancs du dragon */
void Body::init_flancs(GLfloat z_up, GLfloat z_down)
{
    /* Angle courant */
    GLfloat current_angle = ANGLE_MIN * (M_PI/180);

    /* Angle maximal */
    GLfloat max_angle = ANGLE_MAX * (M_PI/180);

    /* Allocation mémoire */
    nbPoints = (GLint) ((max_angle - current_angle)/angle);
    flanc1_up = new GLfloat[nbPoints * 3];
    flanc2_up = new GLfloat[nbPoints * 3];
    flanc1_down = new GLfloat[nbPoints * 3];
    flanc2_down = new GLfloat[nbPoints * 3];

    /* Calcul des points */
    for(GLint i = 0; i < nbPoints * 3; i += 3) {
        flanc1_up[i] = cos(current_angle) * R_LARGE;
        flanc2_up[i] = flanc1_up[i];
        flanc1_down[i] = flanc1_up[i];
        flanc2_down[i] = flanc2_up[i];

        flanc1_up[i + 1] = sin(current_angle) * R_LARGE - DY;
        flanc2_up[i + 1] = - flanc1_up[i + 1];
        flanc1_down[i + 1] = flanc1_up[i + 1];
        flanc2_down[i + 1] = flanc2_up[i + 1];

        flanc1_up[i + 2] = z_up;
        flanc2_up[i + 2] = z_up;
        flanc1_down[i + 2] = z_down;
        flanc2_down[i + 2] = z_down;

        current_angle += angle;
    }
}

static void define_intensity(GLfloat dz_i[], GLint size, GLfloat intensity)
{
    dz_i[0] = intensity;
    dz_i[size - 1] = intensity;
    for(GLint k = 1; k < size / 2; k++) {
        dz_i[k] = dz_i[k - 1] + intensity;
        if(dz_i[k] > 1) {
            dz_i[k] = 1;
        }
        dz_i[size - 1 - k] = dz_i[k];
    }
}

/* Initialisation du dos du dragon */
void Body::init_dos()
{
    /* Allocation mémoire des points nécessaires au dessin du dos */
    dos = new GLfloat*[PREC - 2];
    for(GLint i = 0; i < PREC - 2; i++) {
        dos[i] = new GLfloat[nbPoints * 3];
    }

    /* "Intensité" à appliquer sur z pour obtenir un rendu arrondi */
    GLfloat dz_intensity[PREC - 2];
    define_intensity(dz_intensity, PREC - 2, DOS_DZ_I);

    /* Calcul des points intermédiaires */
    for(GLint i = 0; i < nbPoints * 3; i += 3) {
        // décalage avec y
        GLfloat dy = (flanc1_up[i + 1] - flanc2_up[i + 1])/(PREC - 1);
        // décalage avec z
        GLfloat dz = DZ_DOS;

        for(GLint k = 0; k < PREC - 2; k++) {
            // calcul du décalage
            if(k < (PREC - 2)/2) {
                dz += (DZ_DOS - dz_intensity[k] * DZ_DOS);
            } else {
                dz -= (DZ_DOS - dz_intensity[k] * DZ_DOS);
            }

            dos[k][i] = flanc1_up[i];   // même x
            dos[k][i + 1] = flanc1_up[i + 1] - (k + 1) * dy;    // y décalé
            dos[k][i + 2] = flanc1_up[i + 2] + dz;  // z décalé
        }
    }
}

/* Initialisation du ventre du dragon */
void Body::init_ventre()
{
    /* Allocation mémoire des points nécessaires au dessin du ventre */
    ventre = new GLfloat*[PREC - 2];
    for(GLint k = 0; k < PREC - 2; k++) {
        ventre[k] = new GLfloat[nbPoints * 3];
    }

    /* "Intensité" à appliquer sur z pour obtenir un rendu arrondi */
    GLfloat dz_intensity[PREC - 2];
    define_intensity(dz_intensity, PREC - 2, VENTRE_DZ_I);

    /* Calcul des points intermédiaires */
    for(GLint i = 0; i < nbPoints * 3; i += 3) {
        // décalage avec y
        GLfloat dy = (flanc1_down[i + 1] - flanc2_down[i + 1])/(PREC - 1);
        // décalage avec z
        GLfloat dz = - DZ_VENTRE;

        for(GLint k = 0; k < PREC - 2; k++) {

            ventre[k][i] = flanc1_down[i]; // même x
            ventre[k][i + 1] = flanc1_down[i + 1] - (k + 1) * dy;  // y décalé
            ventre[k][i + 2] = flanc1_down[i + 2] + dz;    // z décalé

            // calcul du décalage
            if(k < (PREC - 2) / 2) {
                dz -= (DZ_VENTRE - dz_intensity[k] * DZ_VENTRE);
            } else {
                dz += (DZ_VENTRE - dz_intensity[k] * DZ_VENTRE);
            }
        }
    }
}

/* Initialisation de l'avant du dragon */
void Body::init_avant()
{
    /* Allocation mémoire des points nécessaires au dessin de l'avant */
    avant = new GLfloat*[PREC];
    for(GLint k = 0; k < PREC; k++) {
        avant[k] = new GLfloat[nbPoints_haut_bas * 3];
    }

    /* "Intensité" à appliquer sur x pour obtenir un rendu arrondi */
    GLfloat dx_intensity[nbPoints_haut_bas - 2];
    define_intensity(dx_intensity, nbPoints_haut_bas - 2, AV_DX_I);
    
    /* Initialisation des premiers et derniers points */
    avant[0][0] = flanc1_up[0];
    avant[0][1] = flanc1_up[1];
    avant[0][2] = flanc1_up[2];
    avant[0][nbPoints_haut_bas * 3 - 3] = flanc1_down[0];
    avant[0][nbPoints_haut_bas * 3 - 2] = flanc1_down[1];
    avant[0][nbPoints_haut_bas * 3 - 1] = flanc1_down[2];
    avant[PREC - 1][0] = flanc2_up[0];
    avant[PREC - 1][1] = flanc2_up[1];
    avant[PREC - 1][2] = flanc2_up[2];
    avant[PREC - 1][nbPoints_haut_bas * 3 - 3] = flanc2_down[0];
    avant[PREC - 1][nbPoints_haut_bas * 3 - 2] = flanc2_down[1];
    avant[PREC - 1][nbPoints_haut_bas * 3- 1] = flanc2_down[2];
    for(GLint k = 1; k < PREC - 1; k++) {
        avant[k][0] = dos[k - 1][0];
        avant[k][1] = dos[k - 1][1];
        avant[k][2] = dos[k - 1][2];
        avant[k][nbPoints_haut_bas * 3- 3] = ventre[k - 1][0];
        avant[k][nbPoints_haut_bas * 3- 2] = ventre[k - 1][1];
        avant[k][nbPoints_haut_bas * 3- 1] = ventre[k - 1][2];
    }

    /* Calcul des points intermédiaires */
    for(GLint k = 0; k < PREC; k++) {
        // décalage avec z
        GLfloat dz = (avant[k][2] - avant[k][nbPoints_haut_bas * 3 - 1])/(nbPoints_haut_bas - 1);
        // décalage avec x
        GLfloat dx = DX_AV;

        for(GLint i = 3; i < nbPoints_haut_bas * 3 - 3; i += 3) {

            if(k == 0 || k == PREC - 1) {
                avant[k][i] = avant[k][i - 3];
            } else {
                avant[k][i] = avant[k][0] + dx;
            }
            avant[k][i + 1] = avant[k][i - 2];
            avant[k][i + 2] = avant[k][i - 1] - dz;
            
            // calcul du décalage
            if(k != 0 &&  i < (nbPoints_haut_bas * 3) / 2) {
                dx += (DX_AV - dx_intensity[i / 3 - 1] * DX_AV);
            } else if(k != 0 && k != PREC - 1) {
                dx -= (DX_AV - dx_intensity[i / 3 - 1] * DX_AV);
            }
        }
    }
}

/* Initialisation de l'arriere du dragon */
void Body::init_arriere()
{
    /* Allocation mémoire des points nécessaires au dessin de l'arrière */
    arriere = new GLfloat*[PREC];
    for(GLint k = 0; k < PREC; k++) {
        arriere[k] = new GLfloat[nbPoints_haut_bas * 3];
    }

    /* "Intensité" à appliquer sur x pour obtenir un rendu arrondi */
    GLfloat dx_intensity[nbPoints_haut_bas - 2];
    define_intensity(dx_intensity, nbPoints_haut_bas - 2, AV_DX_I);
    
    /* Initialisation des premiers et derniers points */
    arriere[0][0] = flanc1_up[nbPoints * 3 - 3];
    arriere[0][1] = flanc1_up[nbPoints * 3 - 2];
    arriere[0][2] = flanc1_up[nbPoints * 3 - 1];
    arriere[0][nbPoints_haut_bas * 3 - 3] = flanc1_down[nbPoints * 3 - 3];
    arriere[0][nbPoints_haut_bas * 3 - 2] = flanc1_down[nbPoints * 3 - 2];
    arriere[0][nbPoints_haut_bas * 3 - 1] = flanc1_down[nbPoints * 3 - 1];
    arriere[PREC - 1][0] = flanc2_up[nbPoints * 3 - 3];
    arriere[PREC - 1][1] = flanc2_up[nbPoints * 3 - 2];
    arriere[PREC - 1][2] = flanc2_up[nbPoints * 3 - 1];
    arriere[PREC - 1][nbPoints_haut_bas * 3 - 3] = flanc2_down[nbPoints * 3 - 3];
    arriere[PREC - 1][nbPoints_haut_bas * 3 - 2] = flanc2_down[nbPoints * 3 - 2];
    arriere[PREC - 1][nbPoints_haut_bas * 3 - 1] = flanc2_down[nbPoints * 3 - 1];
    for(GLint k = 1; k < PREC - 1; k++) {
        arriere[k][0] = dos[k - 1][nbPoints * 3 - 3];
        arriere[k][1] = dos[k - 1][nbPoints * 3 - 2];
        arriere[k][2] = dos[k - 1][nbPoints * 3 - 1];
        arriere[k][nbPoints_haut_bas * 3- 3] = ventre[k - 1][nbPoints * 3 - 3];
        arriere[k][nbPoints_haut_bas * 3- 2] = ventre[k - 1][nbPoints * 3 - 2];
        arriere[k][nbPoints_haut_bas * 3- 1] = ventre[k - 1][nbPoints * 3 - 1];
    }

    /* Calcul des points intermédiaires */
    for(GLint k = 0; k < PREC; k++) {
        // décalage avec z
        GLfloat dz = (arriere[k][2] - arriere[k][nbPoints_haut_bas * 3 - 1])/(nbPoints_haut_bas - 1);
        // décalage avec x
        GLfloat dx = - DX_AV;

        for(GLint i = 3; i < nbPoints_haut_bas * 3 - 3; i += 3) {

            if(k == 0 || k == PREC - 1) {
                arriere[k][i] = arriere[k][i - 3];
            } else {
                arriere[k][i] = arriere[k][0] + dx;
            }
            arriere[k][i + 1] = arriere[k][i - 2];
            arriere[k][i + 2] = arriere[k][i - 1] - dz;
            
            // calcul du décalage
            if(k != 0 &&  i < (nbPoints_haut_bas * 3) / 2) {
                dx -= (DX_AV - dx_intensity[i / 3 - 1] * DX_AV);
            } else if(k != 0 && k != PREC - 1) {
                dx += (DX_AV - dx_intensity[i / 3 - 1] * DX_AV);
            }
        }
    }
}

/* Constructeur */
Body::Body(GLuint texture, GLfloat r, GLfloat g, GLfloat b)
{
    /* Angle entre deux points d'un cercle */
    angle = (360/PRECISION) * (M_PI/180);

    /* Initialisation des flancs */
    init_flancs(Z_UP, Z_DOWN);

    /* Initialisation du dos */
    init_dos();

    /* Initialisation du ventre */
    init_ventre();

    nbPoints_haut_bas = 10;

    /* Initialisation de l'avant */
    init_avant();
    
    /* Initialisation de l'avant */
    init_arriere();

    /* Chargement de la texture */
    tex_peau = texture;

    /* Couleur */
    color_R = r;
    color_G = g;
    color_B = b;
}

/* Destructeur */
Body::~Body()
{
    delete[] flanc1_up;
    delete[] flanc1_down;
    delete[] flanc2_up;
    delete[] flanc2_down;
    for(GLint i = 0; i < PREC - 2; i++) {
        delete[] dos[i];
    }
    delete[] dos;
    for(GLint i = 0; i < PREC - 2; i++) {
        delete[] ventre[i];
    }
    delete[] ventre;
    for(GLint i = 0; i < PREC; i++) {
        delete[] avant[i];
    }
    delete[] avant;
    for(GLint i = 0; i < PREC; i++) {
        delete[] arriere[i];
    }
    delete[] arriere;
}

/* Affichage */
void Body::draw()
{
    glEnable(GL_TEXTURE_2D);
    glColor3f(color_R, color_G, color_B);

    // On utilise la texture peau de serpent
    glBindTexture(GL_TEXTURE_2D, tex_peau);
    // Répétition sur les x
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    // Pas de répétition sur les y
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

    glPushMatrix();
    
    // Dessin du flanc 1
    glBegin(GL_TRIANGLE_STRIP);
        for(GLint i = 0; i < nbPoints * 3; i += 3) {
            // Coordonnées de la texture 2D à projeter
            glTexCoord2d(flanc1_up[i], 0.75);
            // Normale et vecteur du haut
            glNormal3f(flanc1_up[i], flanc1_up[i + 1], flanc1_up[i + 2]);
            glVertex3f(flanc1_up[i], flanc1_up[i + 1], flanc1_up[i + 2]);
            // Coordonnées de la texture 2D à projeter
            glTexCoord2d(flanc1_down[i], 0.25);
            // Normale et vecteur du bas
            glNormal3f(flanc1_down[i], flanc1_down[i + 1], flanc1_down[i + 2]);
            glVertex3f(flanc1_down[i], flanc1_down[i + 1], flanc1_down[i + 2]);
        }
    glEnd();

    // Dessin du flanc 2
    glBegin(GL_TRIANGLE_STRIP);
        for(GLint i = 0; i < nbPoints * 3; i += 3) {
            // Coordonnées de la texture 2D à projeter
            glTexCoord2d(flanc2_up[i], 0.75);
            // Normale et vecteur du haut
            glNormal3f(flanc2_up[i], flanc2_up[i + 1], flanc2_up[i + 2]);
            glVertex3f(flanc2_up[i], flanc2_up[i + 1], flanc2_up[i + 2]);
            // Coordonnées de la texture 2D à projeter
            glTexCoord2d(flanc2_down[i], 0.25);
            // Normale et vecteur du bas
            glNormal3f(flanc2_down[i], flanc2_down[i + 1], flanc2_down[i + 2]);
            glVertex3f(flanc2_down[i], flanc2_down[i + 1], flanc2_down[i + 2]);
        }
    glEnd();

    // Dessin du dos
    draw_dos();

    // Dessin du ventre
    draw_ventre();

    // Suppression répétition sur les x
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);

    // Dessin de l'avant
    draw_avant();

    // Dessin de l'arrière
    draw_arriere();

    glPopMatrix();
    
    glColor3f(1, 1, 1);
    glDisable(GL_TEXTURE_2D);
}

void Body::draw_dos()
{
    // Schéma répété de la moitié du dos (y == 1) jusqu'au flanc (y == 1/2)
    GLfloat tex_dy = 0.5 / ((PREC / 2) + 1);

    // 1) Relié au flanc 1
    glBegin(GL_TRIANGLE_STRIP);
        for(GLint i = 0; i < nbPoints * 3; i += 3) {
            // Coordonnées de la texture 2D à projeter
            glTexCoord2d(flanc1_up[i], 0.5);
            // Normale et vecteur du flanc 1
            glNormal3f(flanc1_up[i], flanc1_up[i + 1], flanc1_up[i + 2]);
            glVertex3f(flanc1_up[i], flanc1_up[i + 1], flanc1_up[i + 2]);
            // Coordonnées de la texture 2D à projeter
            glTexCoord2d(dos[0][i], 0.5 + tex_dy);
            // Normale et vecteur du dos
            glNormal3f(dos[0][i], dos[0][i + 1], dos[0][i + 2]);
            glVertex3f(dos[0][i], dos[0][i + 1], dos[0][i + 2]);
        }
    glEnd();

    GLfloat tex_y = 0.5 + tex_dy;
    // 2) Centre du dos
    for(GLint k = 0; k < PREC - 3; k++) {
        glBegin(GL_TRIANGLE_STRIP);
            for(GLint i = 0; i < nbPoints * 3; i += 3) {
                // Normales et vecteurs pour relier le centre du dos
                
                // Coordonnées de la texture 2D à projeter
                glTexCoord2d(dos[k][i], tex_y);
                glNormal3f(dos[k][i], dos[k][i + 1], dos[k][i + 2]);
                glVertex3f(dos[k][i], dos[k][i + 1], dos[k][i + 2]);

                // Coordonnées de la texture 2D à projeter
                if(k < PREC / 2) {
                    glTexCoord2d(dos[k + 1][i], tex_y + tex_dy);
                } else {
                    glTexCoord2d(dos[k + 1][i], tex_y - tex_dy);
                }
                glNormal3f(dos[k + 1][i], dos[k + 1][i + 1], dos[k + 1][i + 2]);
                glVertex3f(dos[k + 1][i], dos[k + 1][i + 1], dos[k + 1][i + 2]);
            }
            if(k < PREC / 2) {
                tex_y += tex_dy;
            } else {
                tex_y -= tex_dy;
            }
        glEnd();
    }

    // 3) Relié au flanc 2
    glBegin(GL_TRIANGLE_STRIP);
        for(GLint i = 0; i < nbPoints * 3; i += 3) {
            // Coordonnées de la texture 2D à projeter
            glTexCoord2d(dos[PREC - 3][i], 0.5 + tex_dy);
            // Normale et vecteur du dos
            glNormal3f(dos[PREC - 3][i], dos[PREC - 3][i + 1], dos[PREC - 3][i + 2]);
            glVertex3f(dos[PREC - 3][i], dos[PREC - 3][i + 1], dos[PREC - 3][i + 2]);
            // Coordonnées de la texture 2D à projeter
            glTexCoord2d(flanc2_up[i], 0.5);
            // Normale et vecteur du flanc 2
            glNormal3f(flanc2_up[i], flanc2_up[i + 1], flanc2_up[i + 2]);
            glVertex3f(flanc2_up[i], flanc2_up[i + 1], flanc2_up[i + 2]);
        }
    glEnd();
}

void Body::draw_ventre()
{
    // Schéma répété du flanc (y == 1) jusqu'à la moitié du ventre (y == 0)
    GLfloat tex_dy = 1.0 / ((PREC / 2) + 1);

    // 1) Relié au flanc 1
    glBegin(GL_TRIANGLE_STRIP);
        for(GLint i = 0; i < nbPoints * 3; i += 3) {
            // Coordonnées de la texture 2D à projeter
            glTexCoord2d(flanc1_down[i], 1);
            // Normale et vecteur du flanc 1
            glNormal3f(flanc1_down[i], flanc1_down[i + 1], flanc1_down[i + 2]);
            glVertex3f(flanc1_down[i], flanc1_down[i + 1], flanc1_down[i + 2]);
            // Coordonnées de la texture 2D à projeter
            glTexCoord2d(ventre[0][i], 1 - 2*tex_dy);
            // Normale et vecteur du ventre
            glNormal3f(ventre[0][i], ventre[0][i + 1], ventre[0][i + 2]);
            glVertex3f(ventre[0][i], ventre[0][i + 1], ventre[0][i + 2]);
        }
    glEnd();

    GLfloat tex_y = 1 - tex_dy;
    // 2) Centre du ventre
    for(GLint k = 0; k < PREC - 3; k++) {
        glBegin(GL_TRIANGLE_STRIP);
            for(GLint i = 0; i < nbPoints * 3; i += 3) {
                // Normales et vecteurs pour relier le centre du ventre
                
                // Coordonnées de la texture 2D à projeter
                glTexCoord2d(ventre[k][i], tex_y);
                glNormal3f(ventre[k][i], ventre[k][i + 1], ventre[k][i + 2]);
                glVertex3f(ventre[k][i], ventre[k][i + 1], ventre[k][i + 2]);

                // Coordonnées de la texture 2D à projeter
                if(k < PREC / 2) {
                    glTexCoord2d(ventre[k + 1][i], tex_y - tex_dy);
                } else {
                    glTexCoord2d(ventre[k + 1][i], tex_y + tex_dy);
                }
                glNormal3f(ventre[k + 1][i], ventre[k + 1][i + 1], ventre[k + 1][i + 2]);
                glVertex3f(ventre[k + 1][i], ventre[k + 1][i + 1], ventre[k + 1][i + 2]);
            }
            if(k < PREC / 2) {
                tex_y -= tex_dy;
            } else {
                tex_y += tex_dy;
            }
        glEnd();
    }

    // 3) Relié au flanc 2
    glBegin(GL_TRIANGLE_STRIP);
        for(GLint i = 0; i < nbPoints * 3; i += 3) {
            // Coordonnées de la texture 2D à projeter
            glTexCoord2d(ventre[PREC - 3][i], 1 - 2*tex_dy);
            // Normale et vecteur du ventre
            glNormal3f(ventre[PREC - 3][i], ventre[PREC - 3][i + 1], ventre[PREC - 3][i + 2]);
            glVertex3f(ventre[PREC - 3][i], ventre[PREC - 3][i + 1], ventre[PREC - 3][i + 2]);
            // Coordonnées de la texture 2D à projeter
            glTexCoord2d(flanc2_down[i], 1);
            // Normale et vecteur du flanc 2
            glNormal3f(flanc2_down[i], flanc2_down[i + 1], flanc2_down[i + 2]);
            glVertex3f(flanc2_down[i], flanc2_down[i + 1], flanc2_down[i + 2]);
        }
    glEnd();
}

/* Dessin de l'avant */
void Body::draw_avant()
{
    // avant[k][1er point] <=> y = 1 et avant[k][dernier point] <=> y = 0
    GLfloat tex_dy = 1.0 / (nbPoints_haut_bas + 1);
    // avant[0] <=> x = 0 et avant[PREC - 1] <=> x = 1
    GLfloat tex_dx = 1.0 / (PREC + 1);

    GLfloat tex_x = 0;
    for(GLint k = 0; k < PREC - 1; k++) {
        GLfloat tex_y = 1;

        glBegin(GL_TRIANGLE_STRIP);
            for(GLint i = 0; i < nbPoints_haut_bas * 3; i += 3) {
                // Normales et vecteurs pour relier l'avant
                
                // Coordonnées de la texture 2D à projeter
                glTexCoord2d(tex_x, tex_y);
                glNormal3f(avant[k][i], avant[k][i + 1], avant[k][i + 2]);
                glVertex3f(avant[k][i], avant[k][i + 1], avant[k][i + 2]);

                // Coordonnées de la texture 2D à projeter
                glTexCoord2d(tex_x + tex_dx, tex_y);
                glNormal3f(avant[k + 1][i], avant[k + 1][i + 1], avant[k + 1][i + 2]);
                glVertex3f(avant[k + 1][i], avant[k + 1][i + 1], avant[k + 1][i + 2]);
                tex_y -= tex_dy;
            }
        glEnd();
        tex_x += tex_dx;
    }
}

/* Dessin de l'arrière */
void Body::draw_arriere()
{
    // arriere[k][1er point] <=> y = 1 et arriere[k][dernier point] <=> y = 0
    GLfloat tex_dy = 1.0 / (nbPoints_haut_bas + 1);
    // arriere[0] <=> x = 0 et arriere[PREC - 1] <=> x = 1
    GLfloat tex_dx = 1.0 / (PREC + 1);

    GLfloat tex_x = 0;
    for(GLint k = 0; k < PREC - 1; k++) {
        GLfloat tex_y = 1;

        glBegin(GL_TRIANGLE_STRIP);
            for(GLint i = 0; i < nbPoints_haut_bas * 3; i += 3) {
                // Normales et vecteurs pour relier l'arriere

                // Coordonnées de la texture 2D à projeter
                glTexCoord2d(tex_x, tex_y);
                glNormal3f(arriere[k][i], arriere[k][i + 1], arriere[k][i + 2]);
                glVertex3f(arriere[k][i], arriere[k][i + 1], arriere[k][i + 2]);
                
                // Coordonnées de la texture 2D à projeter
                glTexCoord2d(tex_x + tex_dx, tex_y);
                glNormal3f(arriere[k + 1][i], arriere[k + 1][i + 1], arriere[k + 1][i + 2]);
                glVertex3f(arriere[k + 1][i], arriere[k + 1][i + 1], arriere[k + 1][i + 2]);
                tex_y -= tex_dy;
            }
        glEnd();
        tex_x += tex_dx;
    }
}
