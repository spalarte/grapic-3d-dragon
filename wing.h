#ifndef _WING_
#define _WING_

#include "renderable.h"
#include "griffe.h"
#ifndef __APPLE__
#include <GL/glut.h>
#else
#include <GLUT/glut.h>
#endif

class Wing : public Renderable {
    public:
        void draw();
        Wing(float ratio, GLuint tex_griffe, GLfloat r, GLfloat g, GLfloat b,
                GLuint texture_ailes, GLuint texture_cones);
        void animateWing(int amplitude, float vitesse);
    private:
        void drawImmediate();
        float ratio;
        void fillTable();
        void drawCones();
        void animateWingDirection(float vitesse, bool direction);
        GLfloat ver_wing[9][3]; //position de base de l'aile

        //variables pour animation
        double Z_ANGLE;
        bool direction;
        int compteur;

        Griffe * griffe;

        /* Couleur de l'aile */
        GLfloat color_R;
        GLfloat color_G;
        GLfloat color_B;

        /* Texture de l'aile */
        GLuint tex_aile;
        GLuint tex_cones;
};

#endif



