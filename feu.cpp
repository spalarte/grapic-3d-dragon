#include <iostream>
#include <numeric>
using namespace std;
#include "feu.h"

Feu::Feu(float taille) {
	this->taille = taille;
	init();
}

void Feu::init() {
	for (int i = 0; i < MAX_PARTICULES; i++) {
		this->particules[i] = new Particule();
	}
}

void Feu::reinit() {
for (int i = 0; i < MAX_PARTICULES; i++) {
	this->particules[i]->reinit();
	}
}

void Feu::draw() {
	for (int i = 0; i < MAX_PARTICULES; i++) {
		(this->particules[i])->draw();
	}
}

void Feu::animateFeu() {
	for (int i = 0; i < MAX_PARTICULES; i++) {
		(this->particules[i])->animateParticule();
	}
}
