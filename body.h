#ifndef _BODY_
#define _BODY_

#include "renderable.h"
#ifndef __APPLE__
#include <GL/glut.h>
#else
#include <GLUT/glut.h>
#endif

class Body : public Renderable {
    public:
        Body(GLuint texture, GLfloat r, GLfloat g, GLfloat b);
        ~Body();
        void draw();
    private:
        /* Variables utilisées dans le dessin */
        GLfloat angle;
        GLint nbPoints;
        GLint nbPoints_haut_bas;
        GLfloat *flanc1_up;
        GLfloat *flanc2_up;
        GLfloat *flanc1_down;
        GLfloat *flanc2_down;
        GLfloat **dos;
        GLfloat **ventre;
        GLfloat **avant;
        GLfloat **arriere;
        /* Fonctions d'initialisation */
        void init_flancs(GLfloat z_up, GLfloat z_down);
        void init_dos();
        void init_ventre();
        void init_avant();
        void init_arriere();
        /* Fonctions de dessin */
        void draw_dos();
        void draw_ventre();
        void draw_avant();
        void draw_arriere();
        /* Texture */
        GLuint tex_peau;
        /* Couleur */
        GLfloat color_R;
        GLfloat color_G;
        GLfloat color_B;
};

#endif
