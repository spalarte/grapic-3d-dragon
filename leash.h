#ifndef _LEASH_
#define _LEASH_

#include "renderable.h"
#include "particle.h"
#include "spring.h"
#ifndef __APPLE__
#include <GL/glut.h>
#else
#include <GLUT/glut.h>
#endif

class Leash : public Renderable {
public:
    Leash(qglviewer::Vec gravity, double mediumViscosity, double dt);
    void draw();
    void init();
    void animateLeash();
private:
    // system
    std::vector<Particle *> particles;
    std::vector<Spring *> springs;
    // Parameters shared by all particles
    double particleMass;
    double particleRadius;
    double distanceBetweenParticles;

    // Parameters shared by all springs
    double springStiffness;
    double springInitLength;
    double springDamping;

    qglviewer::Vec gravity;
    double mediumViscosity;
    double dt;

    double rebound;

    void createSystemScene();
    void collisionParticleParticle(Particle *p1, Particle *p2);

};

#endif

